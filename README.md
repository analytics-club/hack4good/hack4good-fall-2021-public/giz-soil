# GIZ - ProSoil

## Overview

[GIZ](https://www.giz.de/de/html/index.html) is a German governmental agency that works in international development cooperation. As part of the [Hack4Good](https://analytics-club.org/wordpress/hack4good/) program, they wished to analyse data from their ProSoil program in western Kenya, to promote the adoption of sustainable farming practices by local smallholder farmers.

## How to install dependencies

1. Clone the repository.
2. Inside this folder, create a conda environment using the provided environment definition by running `conda env create -f environment.yml`, NOTE: This can take multiple minutes, if after 5-10 minutes environment has not yet been solved then run the command `conda config --set channel_priority strict` and then rerun the previous command
3. INSIDE the created conda environment run the following command: `pip install kedro-viz`

_Note_: If the environment cannot be solved it can be interesting to go through the packages contained within `environment.yml` and install them one by one. We recommend starting with `gdal`, step 3 should still come last.

## How to run the project

1. Activate the conda environment: `conda activate giz-soil` (see ["How to install dependencies"](#how-to-install-dependencies))
2. Run the default pipeline `kedro run` or a specific one `kedro run --pipeline pipeline_name`. Pipeline definitions, and their names can be found in `pipeline_registry.py` present [here](src/giz_soil/).
3. You can also visualise the default pipeline by running `kedro viz` or a specific one with `kedro viz --pipeline pipeline name`.


## How to use jupyter notebooks

To use jupyter notebooks with Kedro, run `kedro jupyter notebook` or `kedro jupyter lab`, this will allow easy access to datasets present in the data catalog by using `catalog.load("dataset_name_as_in_the_catalog")`.

## Data used

All data is stored in the [`data`](data/) folder, and follows [data engineering convention](https://towardsdatascience.com/the-importance-of-layered-thinking-in-data-engineering-a09f685edc71).

To protect sensitive information, some of the data used in this project is under an NDA. To be able to run the project, the following datasets are expected in the `raw` folder:
- `Beans ass dataset _Dec 2019.csv`
- `Beans assessment dataset _Dec 2018.csv`
- `Beans assessment dataset _July 2019.csv`
- `Bungoma Application - Adoption Survey 2020_WIDE.csv`
- `Kakamega Application - Adoption Survey 2020_WIDE.csv`
- `Kenya_Soil Samples _2015_2019.csv`
- `Maize yield assessment _July_Aug2019 dataset.csv`
- `maize yield assessment_July 2018 dataset.csv`
- `Siaya Application - Adoption Survey 2020.csv`
- `slope.tif`
- `elevation.tif`
- `mean_annual_precipitation_2010_2020.tif`
- `mean_temperature_2010-2020.tif`
- `cation_exchange_capacity_ph7.tif`

## Directory overview
```
.
├── conf                            # Configuration files used by Kedro, untouched in this project
├── data                            # Data used, following data engineering convention
├── logs                            # Automatic logs by Kedro
├── notebooks                       # Jupyter notebooks used for experimentation or final analysis
├── src                             # Source files
│   ├── giz_soil
|   |   ├── extras                  # Custom dataset definitions (geotiff and shp)
|   |   ├── pipelines
|   |   |   ├── data_processing     # Nodes and pipeline definition for data processing
|   |   |   └── data_science        # Nodes and pipeline definition for data science
|   |   └── pipeline_registry.py    # Full pipelines definition
│   └── tests                       # Unit tests, unused in this project
├── .gitignore
├── README.md
└── environment.yml                 # Conda environment definition
```

## Going Further with Kedro

The project uses kedro, to extend it we recommend going through the Spaceflight [tutorial](https://kedro.readthedocs.io/en/stable/03_tutorial/01_spaceflights_tutorial.html). We did not use any advanced features of the framework and everything that was used is should be contained within this tutorial or documented.

## Extending with new TIFS
The TIFF enrichment step is done by using geographical coordinates to look up values in a TIFF. To add a TIFF in the enrichment steps, the following steps should be done:
 - Add the tif in 01_raw
 - Reference it in the [catalog](https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/giz-soil/giz-soil/-/blob/main/conf/base/catalog.yml) (following same structure as other TIFFs).
 - Add it as an input to `merge_geotiffs_node` in the [dataprocessing pipeline](https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/giz-soil/giz-soil/-/blob/main/src/giz_soil/pipelines/data_processing/pipeline.py).

 Note that the following two assumptions must hold:
  - GeoTIFF must be in EPSG4326/WGS84
  - GeoTIFF bounding box must include all potential points that will be looked-up.
