// This script can be used to download climate and topographical data
// through the Google earth engine JavaScript Web API

// area of interest (aoi): rectangle that contains the research area in Kenya
var aoi = ee.Geometry.Polygon(
        [[[33.93629952767708, 1.154436226119246],
          [33.93629952767708, -0.2572279547196924],
          [35.40297433236458, -0.2572279547196924],
          [35.40297433236458, 1.154436226119246]]], null, false);

//-------------------------------------------------
//----------------ERA5 Climate Data----------------
//-------------------------------------------------

//Consider period between 2010-2020
var start_date = ee.Date.fromYMD(2010,1,1)
var end_date = ee.Date.fromYMD(2020, 12, 31)

// Mean annual temperature [Kelvin]
var mean_temp = ee.ImageCollection('ECMWF/ERA5/MONTHLY')
                  .filterDate(start_date, end_date)
                  .filterBounds(aoi)
                  .select("mean_2m_air_temperature").mean();
 
// Mean annual precipitation [m]                 
var mean_annual_precip = ee.ImageCollection('ECMWF/ERA5/MONTHLY')
                  .filterDate(start_date, end_date)
                  .filterBounds(aoi)
                  .select("total_precipitation").sum().divide(11);
                  

//-------------------------------------------------
//----------------ERA5 Climate Data----------------
//-------------------------------------------------
var srtm = ee.Image('CGIAR/SRTM90_V4');
var elevation = srtm.select('elevation');
var slope = ee.Terrain.slope(elevation);

//---------------------EXPORT------------------------------------------
//This will create two export tasks (to Drive) that have to be
//started manually (right side of the editor, Tasks)
//scale is set to 10000m, which is a bit higher than 
//the actual resolution of era5, which is approximately 30000m
Export.image.toDrive({image:mean_temp, 
description:"temperature", 
region:aoi,
scale:10000
})

Export.image.toDrive({image:mean_annual_precip, 
description:"precip", 
region:aoi,
scale:10000
})

Export.image.toDrive({image:elevation, 
description:"elevation",
region:aoi,
scale:30
})

Export.image.toDrive({image:slope, 
description:"slope",
region:aoi,
scale:30
})
