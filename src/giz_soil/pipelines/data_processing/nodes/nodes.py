import pandas as pd
import numpy as np
from typing import Dict

from kedro.config import ConfigLoader
from sklearn.experimental import enable_iterative_imputer  # noqa
from sklearn.impute import SimpleImputer, KNNImputer, IterativeImputer


LAT_COLUMN_NAME = 'cluster_lat'
LON_COLUMN_NAME = 'cluster_lon'

def preprocess_soil_data(soil_data: pd.DataFrame) -> pd.DataFrame:
    """Preprocesses the soil data.

    Args:
        soil_data: Raw data. 
    Returns:
        Preprocessed data.
    """

    # Define interessting rows
    non_negatives_name = ["Cgkg_2015", "Cgkg_2019",  "Ngkg", "Pgkg", "Kmmol+kg", "Cammol+/kg", "Mgmmol+kg", "ZN mgkg", "Cumgkg", "CECmmol+kg", "AgricLimekg", "Compostkg", "Claygkg", "Sandgkg", "Target_yield"]

    # Redefine negative samples as nan
    for name in non_negatives_name: 
        emptymask = [el==" " for el in soil_data[name]]
        soil_data.loc[soil_data.index[emptymask], name]= np.nan

        strmask = [isinstance(el, str) for el in soil_data[name]]
        soil_data.loc[soil_data.index[strmask], name] = soil_data.loc[soil_data.index[strmask], name].astype(float)

        invalid_samples = soil_data[name] < 0 
        soil_data.loc[soil_data.index[invalid_samples], name] = np.nan

    # TODO: conduct z test...?

    return soil_data

def preprocess_adoption_survey(adoption_survey_siaya: pd.DataFrame, adoption_survey_kakamega: pd.DataFrame, 
    adoption_survey_bungoma: pd.DataFrame) -> pd.DataFrame:
    """Preprocesses the adoption survey data.

    Args:
        adoption_survey_siaya: Raw data.
        adoption_survey_kakamega: Raw data.
        adoption_survey_bungoma: Raw data.
    Returns:
        Preprocessed data, with all rows concatenated.
    """
    as_df = pd.concat([adoption_survey_siaya, adoption_survey_kakamega, adoption_survey_bungoma], ignore_index=True)

    # Lose empty columns
    as_df = as_df.dropna(axis=1, how='all')

    # Lose useless columns
    as_df.drop(columns=['Pseudo_ID'])

    # Convert all village names to lowercase
    as_df['village'] = as_df['village'].str.lower()

    # Convert 'yearjoined' to an actual year
    as_df['yearjoined'] = as_df['yearjoined'].apply(lambda n: 2014 + n)

    # Convert the partner info to its actual meaning
    as_df['partner'] = (as_df['partner'].apply(lambda n: {1: 'GOPA/GFA',
                                                          2: 'WHH',
                                                          3: 'UCRC', 
                                                          4: 'ICATIA', 
                                                          5: 'REFSO', 
                                                          6: 'SHIBUYE', 
                                                          7: 'CESUD', 
                                                          8: 'ADS'}[n]))

    # Correctly encode 'not participated' as a 1 in 'benefit_8'
    as_df.at[22, 'benefit_8'] = 1
    as_df.at[210, 'benefit_8'] = 1

    # Encode 'received training' or 'was taught how to make compost' 
    # as 'participated in GIZ ProSoil trainings as a participant'
    # (could have participated as a model farmer also, but we can't know)
    as_df.at[72, 'benefit_2'] = 1
    as_df.at[74, 'benefit_2'] = 1
    as_df.at[239, 'benefit_2'] = 1
    as_df.at[268, 'benefit_2'] = 1

    # Encode 'received sweet potato vines' as 'has received inputs'
    as_df.at[326, 'benefit_4'] = 1
    as_df.at[392, 'benefit_4'] = 1
    as_df.at[393, 'benefit_4'] = 1
    as_df.at[394, 'benefit_4'] = 1

    # Correctly encode row #325 benefit info
    as_df.at[325, 'benefit_7'] = 1
    as_df.at[325, 'benefit_3'] = 1
    as_df.at[325, 'benefit_1'] = 1

    # Drop columns for other benefits as we encoded the info another way
    as_df = as_df.drop(columns=['benefit_888', 'benefit_oth'])

    # Drop neighbour's information as it is not usable
    as_df = as_df.iloc[:, :as_df.columns.get_loc("neighbourtech")]

    return as_df

def preprocess_yield_2018(yield_2018_maize: pd.DataFrame, yield_2018_beans: pd.DataFrame) -> pd.DataFrame:
    """Preprocesses the 2018 yield data.

    Args:
        yield_2018_maize: Raw data.
        yield_2018_beans: Raw data.
    Returns:
        Preprocessed data, with all rows concatenated.
    """
    
    beans_df_2018 = yield_2018_beans[['Pseudo_ID','Unit ID','group_hi5kl13/county', 'group_hi5kl13/sub_county',
                                      'group_hi5kl13/ward','group_hi5kl13/farmer_category','cluster_lat',
                                      'cluster_lon','Cluster','Distance_to_cluster',
                                      'group_hi5kl13/year_engaged_with_project',
                                      'group_yt0lm94/technologies_applied/conservation_agriculture',
                                      'group_yt0lm94/technologies_applied/agroforestry',
                                      'group_yt0lm94/technologies_applied/soil___water_conservation',
                                      'group_yt0lm94/technologies_applied/push_pull',
                                      'group_yt0lm94/technologies_applied/good_agronomic_practices',
                                      'group_yt0lm94/technologies_applied/isf___pest_management',
                                      'group_yt0lm94/technologies_applied/other',
                                      'group_yt0lm94/plot_area_beans','group_yt0lm94/beans_variety',
                                      'group_yt0lm94/cropping_pattern','group_yt0lm94/fertilizer_applied',
                                      'group_yt0lm94/name_fertilizer','group_yt0lm94/seed_rate_use',
                                      'group_om3pb56/beans_yield_measure']].copy()
    
    beans_df_2018.insert(2, 'group', 'hi5kl13')
    beans_df_2018.insert(2, 'time', 2018)
    
    beans_df_2018.rename(columns={'Pseudo_ID': 'pseudo_id',
                                  'Unit ID': 'unit_id',
                                  'group_hi5kl13/county': 'county',
                                  'group_hi5kl13/sub_county': 'sub_county', 
                                  'group_hi5kl13/ward': 'ward',
                                  'group_hi5kl13/farmer_category': 'farmer_category',
                                  'Cluster': 'cluster',
                                  'Distance_to_cluster': 'distance_to_cluster',
                                  'group_hi5kl13/year_engaged_with_project': 'engagement_year',
                                  'group_yt0lm94/technologies_applied/conservation_agriculture': 'conservation_agriculture',
                                  'group_yt0lm94/technologies_applied/agroforestry': 'agroforestry',
                                  'group_yt0lm94/technologies_applied/soil___water_conservation': 'soil_water_conservation',
                                  'group_yt0lm94/technologies_applied/push_pull': 'push_pull',
                                  'group_yt0lm94/technologies_applied/good_agronomic_practices': 'good_agronomic_practices',
                                  'group_yt0lm94/technologies_applied/isf___pest_management': 'integrated_soil_fertility_and_pest_management',
                                  'group_yt0lm94/technologies_applied/other': 'other_technologies',
                                  'group_yt0lm94/plot_area_beans': 'total_area', 
                                  'group_yt0lm94/beans_variety': 'variety',
                                  'group_yt0lm94/cropping_pattern': 'cropping_pattern',
                                  'group_yt0lm94/fertilizer_applied': 'fertilizer_application',
                                  'group_yt0lm94/name_fertilizer': 'fertilizer_type', 
                                  'group_yt0lm94/seed_rate_use': 'seed_rate',
                                  'group_om3pb56/beans_yield_measure': 'yield',
                                 }, inplace=True)
    
    beans_df_2018.insert(0, 'crop', 'beans')
    
    maize_df_2018 = yield_2018_maize[['Pseudo_ID','Unit ID','section_one/county-name',
                                      'section_one/subcounty_name','section_one/ward_name',
                                      'section_one/farmer_category','cluster_lat','cluster_lon','Cluster',
                                      'Distance_to_cluster','section_two/Give_the_year_when_t_gaged_in_the_project',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/Conservation_A',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/Agroforestry',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/Soil_&_water_c',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/Push-pull',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/GAP',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/ISFM',
                                      'section_two/Which_technologies_d_u_apply_in_your_farm/Integrated_Pes',
                                      'section_two/crop_acreage','section_two/crop_var','section_two/crop_pattern',
                                      'section_two/fertilizer_app','section_two/fertilizer_type',
                                      'section_two/seed_rate','section_three/grain_weight_total']].copy()
    
    maize_df_2018.insert(2, 'group', np.nan)
    maize_df_2018.insert(2, 'time', 2018)
    maize_df_2018.insert(18, 'other_technologies', np.nan)
    
    maize_df_2018.rename(columns={'Pseudo_ID': 'pseudo_id',
                                  'Unit ID': 'unit_id',
                                  'section_one/county-name': 'county',
                                  'section_one/subcounty_name': 'sub_county',
                                  'section_one/ward_name': 'ward',
                                  'section_one/farmer_category': 'farmer_category',
                                  'Cluster': 'cluster',
                                  'Distance_to_cluster': 'distance_to_cluster',
                                  'section_two/Give_the_year_when_t_gaged_in_the_project': 'engagement_year',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/Conservation_A': 'conservation_agriculture',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/Agroforestry': 'agroforestry',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/Soil_&_water_c': 'soil_water_conservation',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/Push-pull': 'push_pull',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/GAP': 'good_agronomic_practices',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/ISFM': 'integrated_soil_fertility',
                                  'section_two/Which_technologies_d_u_apply_in_your_farm/Integrated_Pes': 'integrated_pest_management',
                                  'section_two/crop_acreage': 'total_area',
                                  'section_two/crop_var': 'variety',
                                  'section_two/crop_pattern': 'cropping_pattern',
                                  'section_two/fertilizer_app': 'fertilizer_application',
                                  'section_two/fertilizer_type': 'fertilizer_type',
                                  'section_two/seed_rate': 'seed_rate',
                                  'section_three/grain_weight_total': 'yield'
                                 }, inplace=True)

    maize_df_2018.insert(0, 'crop', 'maize')
    maize_df_2018['integrated_soil_fertility_and_pest_management'] = (
        maize_df_2018.apply(lambda row: row['integrated_soil_fertility'] or row['integrated_pest_management'],
                            axis=1))
    maize_df_2018 = maize_df_2018.drop(columns=['integrated_soil_fertility', 'integrated_pest_management'])
    

    preprocessed_yield_2018 = pd.concat([beans_df_2018, maize_df_2018], ignore_index=True)

    # Unify 'cropping_pattern' values
    preprocessed_yield_2018['cropping_pattern'].replace('inter_cropped', 'intercropping', inplace=True)
    preprocessed_yield_2018['cropping_pattern'].replace('pure_cropped', 'pure_cropping', inplace=True)
    preprocessed_yield_2018['cropping_pattern'].replace('pure___cropped', 'pure_cropping', inplace=True)
    
    #Fill NA false assumption validated with Dennis from GIZ
    preprocessed_yield_2018['conservation_agriculture'] = preprocessed_yield_2018['conservation_agriculture'].fillna(False)
    preprocessed_yield_2018['agroforestry'] = preprocessed_yield_2018['agroforestry'].fillna(False)
    preprocessed_yield_2018['soil_water_conservation'] = preprocessed_yield_2018['soil_water_conservation'].fillna(False)
    preprocessed_yield_2018['push_pull'] = preprocessed_yield_2018['push_pull'].fillna(False)
    preprocessed_yield_2018['good_agronomic_practices'] = preprocessed_yield_2018['good_agronomic_practices'].fillna(False)
    preprocessed_yield_2018['integrated_soil_fertility_and_pest_management'] = preprocessed_yield_2018['integrated_soil_fertility_and_pest_management'].fillna(False)
    preprocessed_yield_2018['other_technologies'] = preprocessed_yield_2018['other_technologies'].fillna(False)
    preprocessed_yield_2018['fertilizer_application'] = preprocessed_yield_2018['fertilizer_application'].apply(
        lambda x: {'no': False, 'yes': True, np.nan: np.nan}[x])

    return preprocessed_yield_2018

def preprocess_yield_2019(yield_2019_maize: pd.DataFrame, yield_2019_beans_1: pd.DataFrame, 
                          yield_2019_beans_2: pd.DataFrame) -> pd.DataFrame:
    """Preprocesses the 2019 yield data.

    Args:
        yield_2019_maize: Raw data.
        yield_2019_beans_1: Raw data.
        yield_2019_beans_2: Raw data.
    Returns:
        Preprocessed data, with all rows concatenated.
    """
    
    beans_df_jul_2019 = yield_2019_beans_1[['Pseudo_ID','Unit ID','group_hc6ip72/county','group_hc6ip72/sub_County','group_hc6ip72/ward',
                                          'group_hc6ip72/farmer_category','cluster_lat','cluster_lon','Cluster','Distance_to_cluster',
                                          'group_hc6ip72/year_first_engaged',
                                          'group_ly5oh23/technology_practiced/conservation_agriculture',
                                          'group_ly5oh23/technology_practiced/agroforestry',
                                          'group_ly5oh23/technology_practiced/soil___water_conservation_meas',
                                          'group_ly5oh23/technology_practiced/integrated_soil_fertility___pe',
                                          'group_ly5oh23/technology_practiced/push_pull',
                                          'group_ly5oh23/technology_practiced/good_agronomic_practices',
                                          'group_ly5oh23/technology_practiced/none',
                                          'group_ly5oh23/total_area_beans_acre',
                                          'group_ly5oh23/variety_of_beans',
                                          'group_ly5oh23/cropping_pattern',
                                          'group_ly5oh23/fertilizer_application',
                                          'group_ly5oh23/type_fertilizer',
                                          'group_ly5oh23/seed_rate_used',
                                          'group_fe3jk86/yield_harvested_kg',
                                          'group_ly5oh23/measures_CA/minimum_tilage',
                                          'group_ly5oh23/measures_CA/permanent_soil',
                                          'group_ly5oh23/measures_CA/crop_rotation_', 
                                          'group_ly5oh23/measures_Soil_water_conservati/phyisical',
                                          'group_ly5oh23/measures_Soil_water_conservati/biological',
                                          'group_ly5oh23/measures_Soil_water_conservati/length__reason', 
                                          'group_ly5oh23/measures_agroforestry/variety_of_spe', 
                                          'group_ly5oh23/measures_agroforestry/number_of_tree',
                                          'group_ly5oh23/measures_of_ISF_PM/bilogical_cont',
                                          'group_ly5oh23/measures_of_ISF_PM/composting',
                                          'group_ly5oh23/measures_of_ISF_PM/soil_testing',
                                          'group_ly5oh23/measures_of_ISF_PM/liming', 
                                          'group_ly5oh23/measures_pull_push/bracarie_napei', 
                                          'group_ly5oh23/measures_pull_push/desmodiun',
                                          'group_ly5oh23/measures_pull_push/grass_crop',
                                          'group_ly5oh23/measures_of_GAP/spacing',
                                          'group_ly5oh23/measures_of_GAP/timely_croppin',
                                          'group_ly5oh23/measures_of_GAP/weed___pest_ma',
                                          'group_ly5oh23/measures_of_GAP/minimum_tillag',
                                          'group_ly5oh23/measures_of_GAP/seed_selection']].copy()

    beans_df_dec_2019 = yield_2019_beans_2[['Pseudo_ID','Unit ID','group_hc6ip72/county','group_hc6ip72/sub_County','group_hc6ip72/ward',
                                          'group_hc6ip72/farmer_category','cluster_lat','cluster_lon','Cluster','Distance_to_cluster',
                                          'group_hc6ip72/year_first_engaged',
                                          'group_ly5oh23/technology_practiced/conservation_agriculture',
                                          'group_ly5oh23/technology_practiced/agroforestry',
                                          'group_ly5oh23/technology_practiced/soil___water_conservation_meas',
                                          'group_ly5oh23/technology_practiced/integrated_soil_fertility___pe',
                                          'group_ly5oh23/technology_practiced/push_pull',
                                          'group_ly5oh23/technology_practiced/good_agronomic_practices',
                                          'group_ly5oh23/technology_practiced/none',
                                          'group_ly5oh23/total_area_beans_acre',
                                          'group_ly5oh23/variety_of_beans',
                                          'group_ly5oh23/cropping_pattern',
                                          'group_ly5oh23/fertilizer_application',
                                          'group_ly5oh23/type_fertilizer',
                                          'group_ly5oh23/seed_rate_used',
                                          'group_fe3jk86/yield_harvested_kg', 
                                          'group_ly5oh23/measures_CA/minimum_tilage', 
                                          'group_ly5oh23/measures_CA/permanent_soil', 
                                          'group_ly5oh23/measures_CA/crop_rotation_',
                                          'group_ly5oh23/measures_Soil_water_conservati/phyisical',
                                          'group_ly5oh23/measures_Soil_water_conservati/biological',
                                          'group_ly5oh23/measures_Soil_water_conservati/length__reason',
                                          'group_ly5oh23/measures_agroforestry/variety_of_spe',
                                          'group_ly5oh23/measures_agroforestry/number_of_tree', 
                                          'group_ly5oh23/measures_of_ISF_PM/bilogical_cont',
                                          'group_ly5oh23/measures_of_ISF_PM/composting', 
                                          'group_ly5oh23/measures_of_ISF_PM/soil_testing', 
                                          'group_ly5oh23/measures_of_ISF_PM/liming',  
                                          'group_ly5oh23/measures_pull_push/bracarie_napei', 
                                          'group_ly5oh23/measures_pull_push/desmodiun',  
                                          'group_ly5oh23/measures_pull_push/grass_crop',
                                          'group_ly5oh23/measures_of_GAP/spacing',
                                          'group_ly5oh23/measures_of_GAP/timely_croppin',
                                          'group_ly5oh23/measures_of_GAP/weed___pest_ma',
                                          'group_ly5oh23/measures_of_GAP/minimum_tillag', 
                                          'group_ly5oh23/measures_of_GAP/seed_selection']].copy()
    
    beans_df_jul_2019.insert(2, 'group', 'hc6ip72')
    beans_df_jul_2019.insert(2, 'time', 2019)
    beans_df_jul_2019.insert(36, 'af_good_management', np.nan)
    beans_df_jul_2019.insert(0, 'crop', 'beans')

    beans_df_dec_2019.insert(2, 'group', 'hc6ip72')
    beans_df_dec_2019.insert(2, 'time', 2019)
    beans_df_dec_2019.insert(36, 'af_good_management', np.nan)
    beans_df_dec_2019.insert(0, 'crop', 'beans')
    
    beans_df_2019 = pd.concat([beans_df_jul_2019, beans_df_dec_2019], ignore_index=True)
    
    beans_df_2019.rename(columns={'Pseudo_ID': 'pseudo_id', 
                                  'Unit ID': 'unit_id', 
                                  'group_hc6ip72/county': 'county',
                                  'group_hc6ip72/sub_County': 'sub_county',
                                  'group_hc6ip72/ward': 'ward',
                                  'group_hc6ip72/farmer_category': 'farmer_category', 
                                  'Cluster': 'cluster',
                                  'Distance_to_cluster': 'distance_to_cluster',
                                  'group_hc6ip72/year_first_engaged': 'engagement_year',
                                  'group_ly5oh23/technology_practiced/conservation_agriculture': 'conservation_agriculture',
                                  'group_ly5oh23/technology_practiced/agroforestry': 'agroforestry',
                                  'group_ly5oh23/technology_practiced/soil___water_conservation_meas': 'soil_water_conservation',
                                  'group_ly5oh23/technology_practiced/integrated_soil_fertility___pe': 'integrated_soil_fertility_and_pest_management',
                                  'group_ly5oh23/technology_practiced/push_pull': 'push_pull',
                                  'group_ly5oh23/technology_practiced/good_agronomic_practices': 'good_agronomic_practices',
                                  'group_ly5oh23/technology_practiced/none': 'no_technologies',
                                  'group_ly5oh23/total_area_beans_acre': 'total_area',
                                  'group_ly5oh23/variety_of_beans': 'variety',
                                  'group_ly5oh23/cropping_pattern': 'cropping_pattern',
                                  'group_ly5oh23/fertilizer_application': 'fertilizer_application',
                                  'group_ly5oh23/type_fertilizer': 'fertilizer_type',
                                  'group_ly5oh23/seed_rate_used': 'seed_rate', 
                                  'group_fe3jk86/yield_harvested_kg': 'yield',
                                  'group_ly5oh23/measures_CA/minimum_tilage': 'ca_minimum_tilage',
                                  'group_ly5oh23/measures_CA/permanent_soil': 'ca_permanent_soil',
                                  'group_ly5oh23/measures_CA/crop_rotation_': 'ca_crop_rotation',
                                  'group_ly5oh23/measures_Soil_water_conservati/phyisical': 'swc_physical',
                                  'group_ly5oh23/measures_Soil_water_conservati/biological': 'swc_biological',
                                  'group_ly5oh23/measures_Soil_water_conservati/length__reason': 'swc_reasonable_length',
                                  'group_ly5oh23/measures_agroforestry/variety_of_spe': 'af_variety',
                                  'group_ly5oh23/measures_agroforestry/number_of_tree': 'af_reasonable_nb_tree',
                                  'group_ly5oh23/measures_of_ISF_PM/bilogical_cont': 'isf_pm_biological_control',
                                  'group_ly5oh23/measures_of_ISF_PM/composting': 'isf_pm_composting',
                                  'group_ly5oh23/measures_of_ISF_PM/soil_testing': 'isf_pm_soil_testing',
                                  'group_ly5oh23/measures_of_ISF_PM/liming': 'isf_pm_liming',
                                  'group_ly5oh23/measures_pull_push/bracarie_napei': 'pp_bracarie_napei',
                                  'group_ly5oh23/measures_pull_push/desmodiun': 'pp_desmodiun',
                                  'group_ly5oh23/measures_pull_push/grass_crop': 'pp_grass_crop',
                                  'group_ly5oh23/measures_of_GAP/spacing': 'gap_spacing',
                                  'group_ly5oh23/measures_of_GAP/timely_croppin': 'gap_timely_cropping',
                                  'group_ly5oh23/measures_of_GAP/weed___pest_ma': 'gap_weed_pest_management',
                                  'group_ly5oh23/measures_of_GAP/minimum_tillag': 'gap_minimum_tillage',
                                  'group_ly5oh23/measures_of_GAP/seed_selection': 'gap_seed_selection',
                                 }, inplace=True)
    
    maize_df_2019 = yield_2019_maize[['Pseudo_ID','Unit ID','group_ld4mw96/County_name','group_ld4mw96/sub_county_001',
                                    'group_ld4mw96/ward','group_ld4mw96/farmer_category','cluster_lat','cluster_lon',
                                    'Cluster','Distance_to_cluster','group_ld4mw96/year_first_engaged',
                                    'group_by6ib65/practised_technology/conservation_a','group_by6ib65/practised_technology/agroforestry',
                                    'group_by6ib65/practised_technology/soil___water_c','group_by6ib65/practised_technology/integrated_soi',
                                    'group_by6ib65/practised_technology/push_pull','group_by6ib65/practised_technology/good_agronomic',
                                    'group_by6ib65/practised_technology/none',
                                    'group_by6ib65/farm_area_maize',
                                    'group_by6ib65/maize_variety','group_by6ib65/cropping_pattern',
                                    'group_by6ib65/fertilizer_applicati','group_by6ib65/fertilizer_type',
                                    'group_by6ib65/seed_rate_used',
                                    'group_th5on78/total_weight',
                                    'group_by6ib65/measures_ca/minimum_tilage', 
                                    'group_by6ib65/measures_ca/permanent_soil',
                                    'group_by6ib65/measures_ca/crop_rotation_', 
                                    'group_by6ib65/measures_soil_water_consa/phyisical',
                                    'group_by6ib65/measures_soil_water_consa/biological',
                                    'group_by6ib65/measures_soil_water_consa/reasonable_len',
                                    'group_by6ib65/measures_agroforestry/variety_of_spe',
                                    'group_by6ib65/measures_agroforestry/reasonable_num', 
                                    'group_by6ib65/measures_agroforestry/good_managemen',  
                                    'group_by6ib65/measures_isf_pm/bilogical_cont',
                                    'group_by6ib65/measures_isf_pm/composting',
                                    'group_by6ib65/measures_isf_pm/soil_testing',
                                    'group_by6ib65/measures_isf_pm/liming',
                                    'group_by6ib65/measures_push_pull/bracarie_napei', 
                                    'group_by6ib65/measures_push_pull/desmodiun',
                                    'group_by6ib65/measures_push_pull/grass_crop',
                                    'group_by6ib65/measures_gap/good_spacing',
                                    'group_by6ib65/measures_gap/timely_croppin',
                                    'group_by6ib65/measures_gap/weed___pest_mg',
                                    'group_by6ib65/measures_gap/minimum_tillag',  
                                    'group_by6ib65/measures_gap/seed_selection']].copy()
    
    maize_df_2019.rename(columns={'Pseudo_ID': 'pseudo_id', 
                                  'Unit ID': 'unit_id', 
                                  'group_ld4mw96/County_name': 'county',
                                  'group_ld4mw96/sub_county_001': 'sub_county',
                                  'group_ld4mw96/ward': 'ward',
                                  'group_ld4mw96/farmer_category': 'farmer_category', 
                                  'Cluster': 'cluster',
                                  'Distance_to_cluster': 'distance_to_cluster',
                                  'group_ld4mw96/year_first_engaged': 'engagement_year',
                                  'group_by6ib65/practised_technology/conservation_a': 'conservation_agriculture',
                                  'group_by6ib65/practised_technology/agroforestry': 'agroforestry',
                                  'group_by6ib65/practised_technology/soil___water_c': 'soil_water_conservation',
                                  'group_by6ib65/practised_technology/integrated_soi': 'integrated_soil_fertility_and_pest_management',
                                  'group_by6ib65/practised_technology/push_pull': 'push_pull',
                                  'group_by6ib65/practised_technology/good_agronomic': 'good_agronomic_practices',
                                  'group_by6ib65/practised_technology/none': 'no_technologies',
                                  'group_by6ib65/farm_area_maize': 'total_area',
                                  'group_by6ib65/maize_variety': 'variety',
                                  'group_by6ib65/cropping_pattern': 'cropping_pattern',
                                  'group_by6ib65/fertilizer_applicati': 'fertilizer_application',
                                  'group_by6ib65/fertilizer_type': 'fertilizer_type',
                                  'group_by6ib65/seed_rate_used': 'seed_rate', 
                                  'group_th5on78/total_weight': 'yield',
                                  'group_by6ib65/measures_ca/minimum_tilage': 'ca_minimum_tilage',
                                  'group_by6ib65/measures_ca/permanent_soil': 'ca_permanent_soil',
                                  'group_by6ib65/measures_ca/crop_rotation_': 'ca_crop_rotation',
                                  'group_by6ib65/measures_soil_water_consa/phyisical': 'swc_physical',
                                  'group_by6ib65/measures_soil_water_consa/biological': 'swc_biological',
                                  'group_by6ib65/measures_soil_water_consa/reasonable_len': 'swc_reasonable_length',
                                  'group_by6ib65/measures_agroforestry/variety_of_spe': 'af_variety',
                                  'group_by6ib65/measures_agroforestry/reasonable_num': 'af_reasonable_nb_tree',
                                  'group_by6ib65/measures_isf_pm/bilogical_cont': 'isf_pm_biological_control',
                                  'group_by6ib65/measures_agroforestry/good_managemen': 'af_good_management',
                                  'group_by6ib65/measures_isf_pm/composting': 'isf_pm_composting',
                                  'group_by6ib65/measures_isf_pm/soil_testing': 'isf_pm_soil_testing',
                                  'group_by6ib65/measures_isf_pm/liming': 'isf_pm_liming',
                                  'group_by6ib65/measures_push_pull/bracarie_napei': 'pp_brachiaria_napier',
                                  'group_by6ib65/measures_push_pull/desmodiun': 'pp_desmodiun',
                                  'group_by6ib65/measures_push_pull/grass_crop': 'pp_grass_crop',
                                  'group_by6ib65/measures_gap/good_spacing': 'gap_spacing',
                                  'group_by6ib65/measures_gap/timely_croppin': 'gap_timely_cropping',
                                  'group_by6ib65/measures_gap/weed___pest_mg': 'gap_weed_pest_management',
                                  'group_by6ib65/measures_gap/minimum_tillag': 'gap_minimum_tillage',
                                  'group_by6ib65/measures_gap/seed_selection': 'gap_seed_selection',
                                 }, inplace=True)
    
    maize_df_2019.insert(2, 'group', '6ib65')
    maize_df_2019.insert(2, 'time', 2019)
    maize_df_2019.insert(0, 'crop', 'maize')

    maize_df_2019['engagement_year'].replace(1.75, np.nan, inplace=True)
    maize_df_2019['engagement_year'].replace(2028.00, 2018, inplace=True)
    maize_df_2019['engagement_year'].replace(2105.00, 2015, inplace=True)
    
    preprocessed_yield_2019 = pd.concat([beans_df_2019, maize_df_2019], ignore_index=True)
    preprocessed_yield_2019['cropping_pattern'].replace("inter_cropped", "intercropping", inplace=True)
    preprocessed_yield_2019['cropping_pattern'].replace("pure_cropped", "pure_cropping", inplace=True)
    preprocessed_yield_2019['cropping_pattern'].replace("pure___cropped", "pure_cropping", inplace=True)

    #Fill NA false assumption validated with Dennis from GIZ
    preprocessed_yield_2019['ca_minimum_tilage'] = preprocessed_yield_2019['ca_minimum_tilage'].fillna(False)
    preprocessed_yield_2019['ca_permanent_soil'] = preprocessed_yield_2019['ca_permanent_soil'].fillna(False)
    preprocessed_yield_2019['ca_crop_rotation'] = preprocessed_yield_2019['ca_crop_rotation'].fillna(False)
    preprocessed_yield_2019['swc_physical'] = preprocessed_yield_2019['swc_physical'].fillna(False)
    preprocessed_yield_2019['swc_biological'] = preprocessed_yield_2019['swc_biological'].fillna(False)
    preprocessed_yield_2019['swc_reasonable_length'] = preprocessed_yield_2019['swc_reasonable_length'].fillna(False)
    preprocessed_yield_2019['af_variety'] = preprocessed_yield_2019['af_variety'].fillna(False)
    preprocessed_yield_2019['af_reasonable_nb_tree'] = preprocessed_yield_2019['af_reasonable_nb_tree'].fillna(False)
    preprocessed_yield_2019['isf_pm_biological_control'] = preprocessed_yield_2019['isf_pm_biological_control'].fillna(False)
    preprocessed_yield_2019['af_good_management'] = preprocessed_yield_2019['af_good_management'].fillna(False)
    preprocessed_yield_2019['isf_pm_composting'] = preprocessed_yield_2019['isf_pm_composting'].fillna(False)
    preprocessed_yield_2019['isf_pm_soil_testing'] = preprocessed_yield_2019['isf_pm_soil_testing'].fillna(False)
    preprocessed_yield_2019['isf_pm_liming'] = preprocessed_yield_2019['isf_pm_liming'].fillna(False)
    preprocessed_yield_2019['pp_brachiaria_napier'] = preprocessed_yield_2019['pp_brachiaria_napier'].fillna(False)
    preprocessed_yield_2019['pp_desmodiun'] = preprocessed_yield_2019['pp_desmodiun'].fillna(False)
    preprocessed_yield_2019['pp_grass_crop'] = preprocessed_yield_2019['pp_grass_crop'].fillna(False)
    preprocessed_yield_2019['gap_spacing'] = preprocessed_yield_2019['gap_spacing'].fillna(False)
    preprocessed_yield_2019['gap_timely_cropping'] = preprocessed_yield_2019['gap_timely_cropping'].fillna(False)
    preprocessed_yield_2019['gap_weed_pest_management'] = preprocessed_yield_2019['gap_weed_pest_management'].fillna(False)
    preprocessed_yield_2019['gap_minimum_tillage'] = preprocessed_yield_2019['gap_minimum_tillage'].fillna(False)
    preprocessed_yield_2019['gap_seed_selection'] = preprocessed_yield_2019['gap_seed_selection'].fillna(False)
    preprocessed_yield_2019['fertilizer_application'] = preprocessed_yield_2019['fertilizer_application'].apply(
        lambda x: {'no': False, 'yes': True, np.nan: np.nan}[x])

    return preprocessed_yield_2019

def merge_yield(preprocessed_yield_2018: pd.DataFrame, preprocessed_yield_2019: pd.DataFrame) -> pd.DataFrame:
    """Merge the 2018 and 2019 yield data.

    Args:
        preprocessed_yield_2018: Pre-processed data.
        preprocessed_yield_2019: Pre-processed data.
    Returns:
        Merged data, with columns being the intersection of the two original datasets' columns.
    """
    merged_yield = pd.concat([preprocessed_yield_2018.iloc[:, :28], preprocessed_yield_2019.iloc[:, :28]], ignore_index=True)
    merged_yield = merged_yield.drop(columns=['other_technologies', 'no_technologies'])

    return merged_yield

def merge_yield_and_adoption_survey(merged_yield: pd.DataFrame, preprocessed_adoption_survey: pd.DataFrame) -> pd.DataFrame:
    """Merge the yield and adoption survey data.

    Args:
        merged_yield: Pre-processed data.
        preprocessed_adoption_survey: Pre-processed data.
    Returns:
        Merged data, with columns being the intersection of the two original datasets' columns.
    """
    # Need to take a second look at it with new column names etc (better pre-processing beforehand)
    raise NotImplemented
    # Drop yield columns not present in adoption survey data
    merged_yield = merged_yield.drop(columns=['Pseudo_ID', 'Unit_ID', 'Group', 'Seed_Rate_used',
                                              'Integrated_Soil_Fertility', 'Yield_Harvest_Measure', 
                                              'Cropping_pattern', 'Variety', 'Date_of_Interview', 'Farmer_Category'])

    # Keep only useful columns in preprocessed adoption survey
    preprocessed_adoption_survey = preprocessed_adoption_survey.filter(items=['county', 'scounty', 'ward', 'Cluster', 
                                                'cluster_lat', 'cluster_lon', 'distance_to_cluster_within', 
                                                'nb_years_in_program', 'tenure', 'maize', 'beans', 
                                                'maizebeansintercrop', 'conserveagric', 'agrofostry',
                                                'soil.water', 'pushpull', 'isf.pm', 'isf.pmeasures_6',
                                                'bioferttype', 'gap'])
    
    # Rename columns that do not need any change
    preprocessed_adoption_survey.rename(columns={'county': 'County', 
                                                 'scounty':'Sub_County', 
                                                 'ward': 'Ward',
                                                 'yearjoined': 'Year_of_Engagement',
                                                 'Cluster': 'Cluster_No.', 
                                                 'distance_to_cluster_within': 'Distance_to_cluster',
                                                 'conserveagric': 'conservation_agriculture',
                                                 'agrofostry': 'Agroforestry',
                                                 'soil.water': 'Soil_Water_conservation_meas',
                                                 'pushpull': 'Push_Pull',
                                                 'isf.pm': 'Isf_Pest_Management',
                                                 'isf.pmeasures_6': 'Fertilizer_application',
                                                 'bioferttype': 'Fertilizer_type',
                                                 'gap': 'Good_Agronomic_Practices'})
    
    # Add 'Time' column
    preprocessed_adoption_survey['Time'] = 2020

    # Add 'No_Technology_Practiced/Other' column
    preprocessed_adoption_survey['No_Technology_Practiced/Other'] = False
    preprocessed_adoption_survey.loc[~preprocessed_adoption_survey[['conservation_agriculture',
                                                                    'Agroforestry', 
                                                                    'Soil_Water_conservation_meas', 
                                                                    'Push_Pull', 
                                                                    'Isf_Pest_Management', 
                                                                    'Good_Agronomic_Practices']].any(axis=1),
                                                                    'No_Technology_Practiced/Other'] = True
    
    # Convert the 'maize', 'beans' and 'maizebeansintercrop' info into 'Crop Type' and 'Total Area (Acres)' columns
    columns_as_value_vars = ['maize', 'beans', 'maizebeansintercrop']
    preprocessed_adoption_survey = preprocessed_adoption_survey.melt(
                                    id_vars=[x for x in preprocessed_adoption_survey.columns if x not in columns_as_value_vars],
                                    value_vars=columns_as_value_vars,
                                    var_name='Crop_Type',
                                    value_name='Total_Area (Acres)')
    preprocessed_adoption_survey = preprocessed_adoption_survey.dropna(subset=['Total_Area (Acres)'])

    return pd.concat([merged_yield, preprocessed_adoption_survey], ignore_index=True)

def merge_yield_2019_and_adoption_survey(preprocessed_yield_2019: pd.DataFrame, 
                                         preprocessed_adoption_survey: pd.DataFrame) -> pd.DataFrame:
    """Merge the 2019 yield and adoption survey data.

    Args:
        preprocessed_yield_2019: Pre-processed data.
        preprocessed_adoption_survey: Pre-processed data.
    Returns:
        Merged data, with columns being the intersection of the two original datasets' columns.
    """
    yield_df = preprocessed_yield_2019.drop(columns=['pseudo_id', 'unit_id', 'group', 
                                                     'seed_rate', 'yield', 'cropping_pattern', 
                                                     'variety', 'farmer_category'])
    
    as_df = preprocessed_adoption_survey.filter(
        items=['county', 'scounty', 'ward', 'Cluster', 'cluster_lat', 'cluster_lon', 
               'distance_to_cluster_within', 'yearjoined', 'maize', 
               'beans', 'maizebeansintercrop', 'conserveagric', 'measuresconserv_1',
               'measuresconserv_2', 'measuresconserv_3', 'agrofostry', 'vartrees', 'totaltrees',
               'soil.water', 'soilwaterpra_1', 'soilwaterpra_2', 'soilwaterpra_3', 'isf.pmeasures_1',
               'isf.pmeasures_2', 'isf.pmeasures_3', 'isf.pmeasures_4', 'isf.pmeasures_5', 
               'isf.pmeasures_6', 'isf.pmeasures_7', 'pushpull', 'push.pullmeasure_1', 
               'push.pullmeasure_2', 'push.pullmeasure_3', 'isf.pm','bioferttype', 'gap',
               'gap.measures_1', 'gap.measures_2', 'gap.measures_3', 'gap.measures_4', 'gap.measures_5'])
                    
    as_df.rename(columns={'scounty': 'sub_county',
                          'Cluster': 'cluster',
                          'distance_to_cluster_within': 'distance_to_cluster',
                          'yearjoined': 'engagement_year',
                          'conserveagric': 'conservation_agriculture',
                          'measuresconserv_1': 'ca_minimum_tilage',
                          'measuresconserv_2': 'ca_permanent_soil',
                          'measuresconserv_3': 'ca_crop_rotation',
                          'agrofostry': 'agroforestry', 
                          #TODO: how to transform to boolean
                          #'vartrees': 'af_variety',
                          #TODO: how to transform to boolean
                          #'totaltrees': 'af_reasonable_nb_tree',
                          'soil.water': 'soil_water_conservation', 
                          'soilwaterpra_1': 'swc_physical',
                          'soilwaterpra_2': 'swc_biological',
                          #TODO: check validity of mapping (soilwaterpra_3 = 'cultural measures')
                          #'soilwaterpra_3': 'swc_reasonable_length', 
                          'isf.pm': 'integrated_soil_fertility_and_pest_management', 
                          'isf.pmeasures_1': 'isf_pm_soil_testing',
                          'isf.pmeasures_2': 'isf_pm_composting',
                          'isf.pmeasures_3': 'isf_pm_liming', 
                          #TODO: what mapping (isf.pmeasures_4 = 'cover crop, n-fixing plants')
                          #'isf.pmeasures_4':, 
                          'isf.pmeasures_5': 'isf_pm_biological_control',
                          #TODO: check validity of mapping (soilwaterpra_6 = 'biofertilizer')
                          'isf.pmeasures_6': 'fertilizer_application',
                          #TODO: what mapping (isf.pmeasures_7 = 'biopesticide')
                          #'isf.pmeasures_7', 
                          'pushpull': 'push_pull', 
                          'push.pullmeasure_1': 'pp_brachiaria_napier',
                          'push.pullmeasure_2': 'pp_desmodiun', 
                          'push.pullmeasure_3': 'pp_grass_crop',
                          'bioferttype': 'fertilizer_type',
                          'gap': 'good_agronomic_practices',
                          'gap.measures_1': 'gap_timely_cropping',
                          'gap.measures_2': 'gap_weed_pest_management',
                          'gap.measures_3': 'gap_seed_selection',
                          #TODO: what mapping (gap.measures_4 = 'post harvest management')
                          #'gap.measures_4',
                          #TODO: what mapping (gap.measures_5 = 'residue management')
                          #'gap.measures_5',
                         }, inplace=True)

                        #TODO: find mapping for 'af_good_management', 'gap_spacing', 'gap_minimum_tillage'
    
    #TODO: Why?
    # Add 'time' column
    as_df['time'] = 2020
    
    # Convert the 'maize', 'beans' and 'maizebeansintercrop' info into 'crop' and 'total_area' columns
    columns_as_value_vars = ['maize', 'beans', 'maizebeansintercrop']
    as_df = as_df.melt(id_vars=[x for x in as_df.columns if x not in columns_as_value_vars],
                       value_vars=columns_as_value_vars, var_name='crop',
                       value_name='total_area')
    as_df = as_df.dropna(subset=['total_area'])
    
    #TODO: hope to be able to delete this line
    as_df = as_df[as_df.columns.intersection(yield_df.columns)]
    yield_df = yield_df[yield_df.columns.intersection(as_df.columns)]
    
    merged_yield_2019_and_adoption_survey = pd.concat([as_df, yield_df], ignore_index=True)
    

    #Fill NA false assumption validated with Dennis from GIZ

    merged_yield_2019_and_adoption_survey['ca_minimum_tilage'] = merged_yield_2019_and_adoption_survey['ca_minimum_tilage'].fillna(False)
    merged_yield_2019_and_adoption_survey['ca_permanent_soil'] = merged_yield_2019_and_adoption_survey['ca_permanent_soil'].fillna(False)
    merged_yield_2019_and_adoption_survey['ca_crop_rotation'] = merged_yield_2019_and_adoption_survey['ca_crop_rotation'].fillna(False)
    merged_yield_2019_and_adoption_survey['swc_physical'] = merged_yield_2019_and_adoption_survey['swc_physical'].fillna(False)
    merged_yield_2019_and_adoption_survey['swc_biological'] = merged_yield_2019_and_adoption_survey['swc_biological'].fillna(False)
    # merged_yield_2019_and_adoption_survey['swc_reasonable_length'] = merged_yield_2019_and_adoption_survey['swc_reasonable_length'].fillna(False)
    # merged_yield_2019_and_adoption_survey['af_variety'] = merged_yield_2019_and_adoption_survey['af_variety'].fillna(False)
    # merged_yield_2019_and_adoption_survey['af_reasonable_nb_tree'] = merged_yield_2019_and_adoption_survey['af_reasonable_nb_tree'].fillna(False)
    merged_yield_2019_and_adoption_survey['isf_pm_biological_control'] = merged_yield_2019_and_adoption_survey['isf_pm_biological_control'].fillna(False)
    # merged_yield_2019_and_adoption_survey['af_good_management'] = merged_yield_2019_and_adoption_survey['af_good_management'].fillna(False)
    merged_yield_2019_and_adoption_survey['isf_pm_composting'] = merged_yield_2019_and_adoption_survey['isf_pm_composting'].fillna(False)
    merged_yield_2019_and_adoption_survey['isf_pm_soil_testing'] = merged_yield_2019_and_adoption_survey['isf_pm_soil_testing'].fillna(False)
    merged_yield_2019_and_adoption_survey['isf_pm_liming'] = merged_yield_2019_and_adoption_survey['isf_pm_liming'].fillna(False)
    merged_yield_2019_and_adoption_survey['pp_brachiaria_napier'] = merged_yield_2019_and_adoption_survey['pp_brachiaria_napier'].fillna(False)
    merged_yield_2019_and_adoption_survey['pp_desmodiun'] = merged_yield_2019_and_adoption_survey['pp_desmodiun'].fillna(False)
    merged_yield_2019_and_adoption_survey['pp_grass_crop'] = merged_yield_2019_and_adoption_survey['pp_grass_crop'].fillna(False)
    # merged_yield_2019_and_adoption_survey['gap_spacing'] = merged_yield_2019_and_adoption_survey['gap_spacing'].fillna(False)
    merged_yield_2019_and_adoption_survey['gap_timely_cropping'] = merged_yield_2019_and_adoption_survey['gap_timely_cropping'].fillna(False)
    merged_yield_2019_and_adoption_survey['gap_weed_pest_management'] = merged_yield_2019_and_adoption_survey['gap_weed_pest_management'].fillna(False)
    # merged_yield_2019_and_adoption_survey['gap_minimum_tillage'] = merged_yield_2019_and_adoption_survey['gap_minimum_tillage'].fillna(False)
    merged_yield_2019_and_adoption_survey['gap_seed_selection'] = merged_yield_2019_and_adoption_survey['gap_seed_selection'].fillna(False)

    return merged_yield_2019_and_adoption_survey.applymap(lambda x: int(x) if(type(x) == bool) else x)

def create_yield_prediction_model_input(merged_yield_enriched: pd.DataFrame) -> pd.DataFrame:
    """Create the yield prediction model input data.

    Args:
        merged_yield_enriched: data.
    Returns:
        Clean data with no nan values.
    """
    yield_prediction_model_input = merged_yield_enriched.copy()

    # Drop unwanted columns
    yield_prediction_model_input = yield_prediction_model_input.drop(columns=['pseudo_id', 
                                                                              'unit_id', 
                                                                              'group', 
                                                                              'county',
                                                                              'sub_county',
                                                                              'ward',
                                                                              'farmer_category',
                                                                              'cluster_lat',
                                                                              'cluster_lon',
                                                                              'cluster',
                                                                              'distance_to_cluster',
                                                                              'variety',
                                                                              'fertilizer_type',
                                                                              'time',
                                                                              'engagement_year',
                                                                             ])                                                                     

    # Impute missing values

    ## Imputing farming practices with False
    farming_practices = ['conservation_agriculture', 
                         'agroforestry', 
                         'soil_water_conservation', 
                         'push_pull', 
                         'good_agronomic_practices', 
                         'integrated_soil_fertility_and_pest_management', 
                         'fertilizer_application']
    yield_prediction_model_input[farming_practices] = yield_prediction_model_input[farming_practices].fillna(False)

    ## Imputing other categorical values with 'most frequent' strategy
    other_cat_features = [#'time',
                        #   'engagement_year',
                          'cropping_pattern',
                          'crop',
                        #   'county',
                        #   'farmer_category',
                         ]
    
    yield_prediction_model_input[other_cat_features] = SimpleImputer(strategy='most_frequent').fit_transform(
        yield_prediction_model_input[other_cat_features])

    ## Get dummies to be able to run other imputers
    # yield_prediction_model_input['engagement_year'] = yield_prediction_model_input['engagement_year'].astype(str)
    yield_prediction_model_input = pd.get_dummies(yield_prediction_model_input,
                                                  columns=[#'time',
                                                        #    'engagement_year',
                                                           'cropping_pattern',
                                                           'crop',
                                                        #    'county',
                                                        #    'farmer_category',
                                                          ])     

    ## Imputing continuous features
    columns = yield_prediction_model_input.columns
    yield_prediction_model_input = IterativeImputer(random_state=0, verbose=2).fit_transform(yield_prediction_model_input)
    yield_prediction_model_input = pd.DataFrame(yield_prediction_model_input, columns=columns)
    
    # Drop all rows with non-imputed missing values
    # yield_prediction_model_input = yield_prediction_model_input.dropna()
    
    return yield_prediction_model_input

def create_farming_practices_clustering_model_input(merged_yield_2019_and_adoption_survey_enriched: pd.DataFrame) -> pd.DataFrame:
    """Create the farming practices clustering model input data.

    Args:
        merged_yield_2019_and_adoption_survey: data.
    Returns:
        Clean data with no nan values.
    """
    farming_practices_clustering_model_input = merged_yield_2019_and_adoption_survey_enriched.copy()

    farming_practices_clustering_model_input = farming_practices_clustering_model_input.filter(['ca_minimum_tilage',
                                                                                                'ca_permanent_soil',
                                                                                                'ca_crop_rotation',
                                                                                                'swc_physical',
                                                                                                'swc_biological',
                                                                                                'isf_pm_biological_control',
                                                                                                'isf_pm_composting',
                                                                                                'isf_pm_soil_testing',
                                                                                                'isf_pm_liming',
                                                                                                'pp_brachiaria_napier',
                                                                                                'pp_desmodiun',
                                                                                                'pp_grass_crop',
                                                                                                'gap_timely_cropping',
                                                                                                'gap_weed_pest_management',
                                                                                                'gap_seed_selection'])

    farming_practices_clustering_model_input.fillna(0, inplace=True)
    
    # # Drop unwanted columns
    # farming_practices_clustering_model_input.drop(columns={'county',
    #                                                        'sub_county',
    #                                                        'ward',
    #                                                        'cluster_lat',
    #                                                        'cluster_lon',
    #                                                        'cluster',
    #                                                        'distance_to_cluster',
    #                                                        'fertilizer_type'
    #                                                       }, inplace=True)
    
    # # Impute missing values

    # ## Imputing farming practices with False
    # farming_practices = ['conservation_agriculture', 
    #                      'agroforestry', 
    #                      'soil_water_conservation', 
    #                      'push_pull', 
    #                      'good_agronomic_practices', 
    #                      'integrated_soil_fertility_and_pest_management', 
    #                      'fertilizer_application']
    # print(farming_practices_clustering_model_input.columns)
    # farming_practices_clustering_model_input[farming_practices] = farming_practices_clustering_model_input[farming_practices].fillna(False)

    # # columns = farming_practices_clustering_model_input.columns
    # # farming_practices_clustering_model_input = (KNNImputer(n_neighbors=5, weights='uniform', metric='nan_euclidean')
    # #     .fit_transform(farming_practices_clustering_model_input))
    # # farming_practices_clustering_model_input = pd.DataFrame(farming_practices_clustering_model_input, columns)
    
    # # Drop all rows with non-imputed missing values
    # #farming_practices_clustering_model_input = farming_practices_clustering_model_input.dropna()

    # print(farming_practices_clustering_model_input.info())
    return farming_practices_clustering_model_input
