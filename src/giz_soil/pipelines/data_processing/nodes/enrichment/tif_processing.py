from numpy import array
import rasterio as rs
import pandas as pd
from typing import Dict, Tuple
import numpy as np
from pathlib import PurePosixPath

def combine_geotiffs(*args: rs.io.DatasetReader) -> Dict[str, rs.io.DatasetReader]:
    """Combines multiple dataset readers into a single dictionnary.

    Args: 
        *args: Variable argument number, each one should be a reader on a geotiff.

    Returns:
        Dictionary where the key is the name extracted from the file and the value is the reader.
        Note: No deep copy is executed, it is possible to modify the original datasets with the return values.
    """

    datasets = {}
    for ds in args:
        #Extracting the name
        name = PurePosixPath(ds.name).name.split('.')[0]
        datasets[name] = ds

    return datasets

def enrich_with_geotiff_data(dataframe_with_lat_and_lon: pd.DataFrame, lon_name: str, lat_name: str, enriching_tifs: Dict[str, rs.io.DatasetReader]) -> pd.DataFrame:

    """Enrich the chosen dataframe with information from the GeoTIFF data. The chosen dataset
    needs to have geographical information in the form of latitude and longitude for each row.

    Args:
        dataframe_with_lat_and_lon: Any dataset with latitude and longitude features.
        geotiff_data: Collection with the name of the tif and the a Reader to the tif.
    Returns:
        Enriched data, with columns being the original ones plus the added GeoTIFF info.

    Assumptions: This functions uses the tifs dataset, it assumes the following: 
        - All points fetched are contained into the tifs.
        - The tifs in the Datacatalogue contain only one layer
        - All tifs are in ESPG:4326 format. Behavior is undefined if this is not the case
    """

    #TODO - Future improvements: Add arg to specify columns to add, support for multi-layer tif, very bounds of tif

    enriching_tiffs_with_data  = {name: [reader, reader.read(1)] for name, reader in enriching_tifs.items()}
    
    def enrich_row(row: pd.Series):
        new_elements = pd.Series(dtype = float)

        #For every enriching tiff we add the column to the series
        for name, (tif, data) in enriching_tiffs_with_data.items():
         
            #Longitude: x, Latitude: y
            new_elements = new_elements.append(pd.Series([data[tif.index(row[lon_name],row[lat_name])]], index=[name]))

        new_row = pd.concat([row, new_elements], axis = 0)

        #returning the new row 
        return new_row

    #We map all columns to the new row
    df_enriched = dataframe_with_lat_and_lon.apply(enrich_row, axis= 1)

    return df_enriched    
