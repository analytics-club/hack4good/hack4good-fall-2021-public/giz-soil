import pandas as pd
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor

def enrich_with_soil_data(dataframe_with_lat_and_lon: pd.DataFrame, soil_giz: pd.DataFrame,
    lon_name: str, lat_name:str) -> pd.DataFrame:
    """Enrich the chosen dataframe with soil information from the GIZ soil data. The chosen dataset
    needs to have geographical information in the form of latitude and longitude for each row.

    Args:
        dataframe_with_lat_and_lon: Any dataset with latitude and longitude features.
        soil_giz: Raw data.
        lon_name: Name of the column containing the longitude info
        lat_name: Name of the column containing the latitude info
    Returns:
        Enriched data, with columns being the original ones plus the added soil info.
    """

    __important_measurements = ['cluster_lat', 'cluster_lon', 'pH_2015', 'pH_2019',
        'Cgkg_2015', 'Cgkg_2019', 'Ngkg', 'Pgkg', 'Kmmol+kg', 'Cammol+/kg']
    __values_to_transfer = ['pH_2015', 'pH_2019',
        'Cgkg_2015', 'Cgkg_2019', 'Ngkg', 'Pgkg', 'Kmmol+kg', 'Cammol+/kg']

    def make_predictors(): 
        """
        Takes the soil dataset and returns a collection of predictors.
        These take geographical coordinate and return the interpolated value for it (feature depending on predictor).
        """
        
        #We average all soild characteristics within the same cluster
        dfmeans = soil_giz[['Cluster'] + __important_measurements].groupby(by = 'Cluster').mean() 

        #We collect all coordinate points from the soil dataset
        x = dfmeans['cluster_lon']
        y = dfmeans['cluster_lat']
        points = np.vstack([x.to_numpy(), y.to_numpy()]).T

        GP_Regressors = {}
        #For each of the interpolated features we fit our points
        #with the corresponding z
        for key in __values_to_transfer:

            z = dfmeans[key].to_numpy()

            GP = GaussianProcessRegressor(random_state=42)
            GP.fit(points, z)
            
            GP_Regressors[key] = GP
        return GP_Regressors
    
    predictors = make_predictors()

    #We fetch all x,y coordinates on the dataset to enrich
    xpred = dataframe_with_lat_and_lon[lon_name]
    ypred = dataframe_with_lat_and_lon[lat_name]

    #We filter out all points with a null x or y coordinate
    xmask = xpred.isnull()
    ymask = ypred.isnull()
    mask = (~xmask)*(~ymask)

    points = np.vstack([xpred.to_numpy(), ypred.to_numpy()]).T
    #Selecting the points
    mpoints = points[mask]
    
    res = dataframe_with_lat_and_lon.copy()
    for value_key in __values_to_transfer:
        
        #Points with x or y null will be predicted nan
        pred_value = np.empty(xpred.shape)
        pred_value[:] = np.nan

        m_pred_value = predictors[value_key].predict(mpoints)

        #Assigning the result in the valid points
        pred_value[mask] = m_pred_value
        res[value_key] = pred_value
    
    return res
