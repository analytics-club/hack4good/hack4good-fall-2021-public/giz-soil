import rasterio as rs
from .soil_processing import enrich_with_soil_data
from .tif_processing import enrich_with_geotiff_data
from typing import Dict
import pandas as pd


def enrich_with_all_geographical_data(dataframe_with_lat_and_lon: pd.DataFrame, lon_name: str, lat_name: str,
    soil_giz: pd.DataFrame, enriching_tifs: Dict[str, rs.io.DatasetReader])-> pd.DataFrame:
    """Enrich the chosen dataframe with information from all the geographical datasets. The chosen dataset
    needs to have geographical information in the form of latitude and longitude for each row.

    Args:
        dataframe_with_lat_and_lon: Any dataset with latitude and longitude features.
        soil_giz: Raw data.
        geotiff_data: Raw data.
    Returns:
        Enriched data, with columns being the original ones plus the added geographical info.
    """
    return enrich_with_geotiff_data(enrich_with_soil_data(dataframe_with_lat_and_lon, soil_giz, lon_name, lat_name), lon_name, lat_name, enriching_tifs)