from re import sub
import geopandas as gpd
import numpy as np
import pandas as pd
from difflib import SequenceMatcher

def add_missing_clusters(df_missing_lat_lon: pd.DataFrame, subcounty_data: gpd.GeoDataFrame, lon_name: str, lat_name: str) -> pd.DataFrame: 
    """
    Function computes missing geographical coordinate using the 'Ward' field.
    Elements that have no geographical data and no subcounty info are dropped.

    Arguments:
        df_missing_lat_lon: DataFrame with columns for longitute, lattitude and subcounty.
        subcounty_data: GeoDataFrame containing coordinate information for every subcounty in Kenya. 
        lon_name: Column containing the longitude in `df_missing_lat_lon`
        lat_name: Column containing the latitude in `df_missing_lat_lon`
    
    Returns:
       Same DataFrame with missing geograpgical data completed with subcounty data. Rows without subcounty data are dropped.

    """

    #Dictionary containing a mapping from (county_name -> (long, lat))
    #Multiple rows will have the same 'Sub_County' value hence the same inferred long and lat.
    coordinates_for_subcounty = {}

    def add_coord(row: pd.Series) -> pd.Series:
        """
        Takes a row and adds geo info using the Subcounty info. Assumes there is no nan in subcounty.
        """

        subcounty_to_search = row['sub_county']

        #First row we encounter with this subcounty
        if(not (subcounty_to_search in coordinates_for_subcounty)):

            #We map every subcounty from subcounty_data to a similarity score with the subcounty of our row.
            #Note this might overwrite outdated contents already stored in 'Similarity'

            subcounty_data['Similarity'] = subcounty_data['subcounty'].apply(lambda w: SequenceMatcher(a=__standardize_subcounty_name(w), b=__standardize_subcounty_name(subcounty_to_search)).ratio())

            #Check if we have a subcounty that contains the name we are looking for or reverse
            subcounty_data['Contains_Subcounty'] = subcounty_data['subcounty'].apply(lambda w: __standardize_subcounty_name(w) in __standardize_subcounty_name(subcounty_to_search) or  __standardize_subcounty_name(subcounty_to_search) in __standardize_subcounty_name(w))

            selected_subcounties = subcounty_data[subcounty_data['Contains_Subcounty'] == True]

            #TODO: 
            idx_most_similar_row = None

            #The subcounty we are looking for is a substring of something else, we take the most similar among those that contains it those it contains
            if(not selected_subcounties.empty and not __subcounty_has_wrong_match(subcounty_to_search)):
                relevant_columns = selected_subcounties[['Center_Lon', 'Center_Lat', 'Similarity']]
                idx_most_similar_row = relevant_columns['Similarity'].idxmax()
                subcounty_center_long = relevant_columns.loc[idx_most_similar_row]['Center_Lon']
                subcounty_center_lat = relevant_columns.loc[idx_most_similar_row]['Center_Lat']

                #We save the results for future use
                coordinates_for_subcounty[subcounty_to_search] = [subcounty_center_long, subcounty_center_lat]



            #If we could not find matching or it is one of the registered names to be deiscared.
            else: 
                #We store nans, we will discard all rows that still contain nans before returning the dataframe
                coordinates_for_subcounty[subcounty_to_search] = [np.nan, np.nan]

            


        row[lon_name] = coordinates_for_subcounty[subcounty_to_search][0]
        row[lat_name] = coordinates_for_subcounty[subcounty_to_search][1]
        return row


    #Mask for rows already containing geographical data
    rows_geo_info = (~(df_missing_lat_lon[lon_name].isnull()) & ~(df_missing_lat_lon[lat_name].isnull()))

    #Mask for rows with no geographical data but with subcounty data
    rows_to_map = (df_missing_lat_lon[lon_name].isnull() | df_missing_lat_lon[lat_name].isnull()) & (~df_missing_lat_lon['sub_county'].isnull())

    #We add the geographical info
    df_missing_lat_lon[rows_to_map] = df_missing_lat_lon[rows_to_map].apply(add_coord, axis = 1)

    df = df_missing_lat_lon[rows_geo_info | rows_to_map]

    #Return everything except rows with no geo info and no subcounty and the rows that could not be added coordinates during apply
    return df[~df['cluster_lon'].isna() & ~df['cluster_lat'].isna()]

#Add any subcounty identified to be mapped wrong to the logical expression
def __subcounty_has_wrong_match(sc: str) -> bool:
    return sc == 'E'

def __standardize_subcounty_name(s: str):
    #we delete noise to keep only the core of the subcounty name
    return s.lower().replace('north', '').replace('south', '').replace('west', '').replace('east', '').replace('central', '').replace('subcounty', '').replace('sub county', '').replace("'", '').replace('.', '').strip()

def compute_centroid(gdf: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """
    Computes all centroids of a GeoDataFrame containing a geometry column

    Argument:
        gdf: GeoDataframe containing a 'geometry' for every row.

    Returns:
        New GeoDataframe with the coordinates of the centroid in column 'Center_Lon' and 'Center_Lat'
    """

    #The centroid method only functions in flat geometry.
    #On size of Kenya errors are minimal but would increase with greater surface.
    centers = gdf.to_crs(epsg=3395).centroid.to_crs(epsg=4326)
    gdf['Center_Lon'] = centers.x
    gdf['Center_Lat'] = centers.y

    #The centroid method only functions in flat geometry.
    #On size of Kenya errors are minimal but would increase with greater surface.
    return gdf
