from kedro.pipeline import Pipeline, node

from .nodes.nodes import *

from .nodes.enrichment.tif_processing import combine_geotiffs
from .nodes.enrichment.combined import enrich_with_all_geographical_data
from .nodes.enrichment.missing_cluster import compute_centroid, add_missing_clusters

def create_pipeline(result: str):
   
    #This node does relatively expensive computations hence it exists to cache the results.
    compute_subcounties_centroid_node = node(
        func=compute_centroid,
        inputs='kenya_subcounties',
        outputs='kenya_subcounties_centroid_coordinates',
        name='compute_subcounties_centroid',
    )
    
    complete_geo_data_merged_yield_node = node(
        func=add_missing_clusters,
        inputs=['merged_yield','kenya_subcounties_centroid_coordinates', 'params:long_name', 'params:lat_name'],
        outputs ='merged_yield_complete_geo_data',
        name ='complete_geo_data_merged_yield',
    )

    complete_geo_data_merged_yield_2019_and_adoption_survey_node = node(
        func=add_missing_clusters,
        inputs=['merged_yield_2019_and_adoption_survey','kenya_subcounties_centroid_coordinates', 'params:long_name', 'params:lat_name'],
        outputs ='merged_yield_2019_and_adoption_survey_complete_geo_data',
        name ='complete_geo_data_merged_yield_2019_and_adoption_survey',
    )

    merge_geotiffs_node = node(
        func=combine_geotiffs,
        inputs=["slope_geotiff", "elevation_geotiff", "precipitation_geotiff", "temperature_geotiff", "cation_exchange_geotiff"],
        outputs = "merged_geotiffs",
        name = "merge_geotiffs",
    )

    preprocess_adoption_survey_node = node(
        func=preprocess_adoption_survey,
        inputs=["adoption_survey_siaya", "adoption_survey_kakamega", "adoption_survey_bungoma"],
        outputs="preprocessed_adoption_survey",
        name="preprocess_adoption_survey",
    )

    preprocess_yield_2018_node = node(
        func=preprocess_yield_2018,
        inputs=["yield_2018_maize", "yield_2018_beans"],
        outputs="preprocessed_yield_2018",
        name="preprocess_yield_2018",
    )

    preprocess_yield_2019_node = node(
        func=preprocess_yield_2019,
        inputs=["yield_2019_maize", "yield_2019_beans_1", "yield_2019_beans_2"],
        outputs="preprocessed_yield_2019",
        name="preprocess_yield_2019",
    )

    merge_yield_node = node(
        func=merge_yield,
        inputs=["preprocessed_yield_2018", "preprocessed_yield_2019"],
        outputs="merged_yield",
        name="merge_yield",
    )

    merge_yield_and_adoption_survey_node = node(
        func=merge_yield_and_adoption_survey,
        inputs=["merged_yield", "preprocessed_adoption_survey"],
        outputs="merged_yield_and_adoption_survey",
        name="merge_yield_and_adoption_survey",
    )

    merge_yield_2019_and_adoption_survey_node = node(
        func=merge_yield_2019_and_adoption_survey,
        inputs=["preprocessed_yield_2019", "preprocessed_adoption_survey"],
        outputs="merged_yield_2019_and_adoption_survey",
        name="merge_yield_2019_and_adoption_survey",
    )

    enrich_merged_yield_node = node(
        func=enrich_with_all_geographical_data,
        inputs=["merged_yield_complete_geo_data", "params:long_name", "params:lat_name", "preprocessed_soil_data", "merged_geotiffs"],
        outputs="merged_yield_enriched",
        name="enrich_merged_yield",
    )

    enrich_preprocessed_adoption_survey_node = node(
        func=enrich_with_all_geographical_data,
        inputs=["preprocessed_adoption_survey", "params:long_name", "params:lat_name", "soil_giz", "merged_geotiffs"],
        outputs="preprocessed_adoption_survey_enriched",
        name="enrich_preprocessed_adoption_survey",
    )

    enrich_merged_yield_and_adoption_survey_node = node(
        func=enrich_with_all_geographical_data,
        inputs=["merged_yield_and_adoption_survey", "params:long_name", "params:lat_name", "soil_giz", "merged_geotiffs"],
        outputs="merged_yield_and_adoption_survey_enriched",
        name="enrich_merged_yield_and_adoption_survey",
    )

    enrich_merged_yield_2019_and_adoption_survey_node = node(
        func=enrich_with_all_geographical_data,
        inputs=["merged_yield_2019_and_adoption_survey_complete_geo_data", "params:long_name", "params:lat_name", "preprocessed_soil_data", "merged_geotiffs"],
        outputs="merged_yield_2019_and_adoption_survey_enriched",
        name="enrich_merged_yield_2019_and_adoption_survey",
    )

    create_yield_prediction_model_input_node = node(
        func=create_yield_prediction_model_input,
        inputs=["merged_yield_enriched"],
        outputs="yield_prediction_model_input",
        name="create_yield_prediction_model_input",
    )

    create_farming_practices_clustering_model_input_node = node(
        func=create_farming_practices_clustering_model_input,
        inputs=["merged_yield_2019_and_adoption_survey_enriched"],
        outputs="farming_practices_clustering_model_input",
        name="create_farming_practices_clustering_model_input",
    )
    
    preprocess_soil_data_node = node(
        func=preprocess_soil_data,
        inputs=["soil_giz"],
        outputs="preprocessed_soil_data",
        name="preprocess_soil_data",
    )

    if result == "yield_prediction_model_input":
        return Pipeline(
            [
                preprocess_yield_2018_node,
                preprocess_yield_2019_node,
                merge_yield_node,
                preprocess_soil_data_node,
                merge_geotiffs_node,
                enrich_merged_yield_node,
                compute_subcounties_centroid_node,
                complete_geo_data_merged_yield_node,
                create_yield_prediction_model_input_node
            ]
        )
    elif result == "farming_practices_clustering_model_input":
        return Pipeline(
            [
                preprocess_adoption_survey_node,
                preprocess_yield_2019_node,
                merge_yield_2019_and_adoption_survey_node,
                compute_subcounties_centroid_node,
                complete_geo_data_merged_yield_2019_and_adoption_survey_node,
                preprocess_soil_data_node,
                merge_geotiffs_node,
                enrich_merged_yield_2019_and_adoption_survey_node,
                create_farming_practices_clustering_model_input_node,
            ]
        )
    else:
        raise ValueError