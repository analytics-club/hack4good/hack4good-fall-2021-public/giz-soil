import logging
from typing import Dict, Tuple

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Lasso
from sklearn.linear_model import Lasso, ElasticNet
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor

def split_data(data: pd.DataFrame, parameters: Dict) -> Tuple[pd.DataFrame, pd.DataFrame, pd.Series, pd.Series]:
    """Splits data into features and targets training and test sets.

    Args:
        data: Data containing features and target.
        parameters: Parameters defined in parameters.yml.
    Returns:
        Split data.
    """
    X = data[[x for x in data.columns if x not in parameters['outputs']]]
    y = data[parameters['outputs']]
    X_train, X_test, y_train, y_test = train_test_split(
        pd.get_dummies(X), y, test_size=parameters['test_size'], random_state=parameters['random_state']
    )
    return X_train, X_test, y_train, y_test

def normalize_data(X_train: pd.DataFrame, X_test: pd.DataFrame, parameters: Dict) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Normalize X_train and X_test only using X_train's data.

    Args:
        X_train: Training data.
        X_test: Test data.
    Returns:
        Tuple of normalized data.
    """
    cont_features = parameters['continuous_features']
    cat_features = [x for x in X_train.columns if x not in cont_features]

    # Normalize continuous values
    scaler = StandardScaler()
    scaler.fit(X_train[cont_features])
    X_train.loc[:, cont_features] = scaler.transform(X_train[cont_features])
    X_test.loc[:, cont_features] = scaler.transform(X_test[cont_features])

    # Map binary classes to {-1, 1} (all multiclass was supposedly previously dummified)
    X_train.loc[:, cat_features] = X_train.loc[:, cat_features].apply(lambda c: c.map({0: -1, 1: 1}), axis=1)
    X_test.loc[:, cat_features] = X_test.loc[:, cat_features].apply(lambda c: c.map({0: -1, 1: 1}), axis=1)
    
    return X_train, X_test

def grid_search_lasso(X_train: pd.DataFrame, y_train: pd.Series) -> GridSearchCV:
    """Trains the lasso regression model through a gridsearch crossvalidation to get
    the best hyper parameters.

    Args:
        X_train: Training data of features.
        y_train: Training data for output.

    Returns:
        Trained model.
    """
    gs_params = {
        'alpha': np.logspace(-1, 1, 10)
    }
    gs = GridSearchCV(Lasso(), gs_params, cv=5, verbose=3)
    gs.fit(X_train, y_train)

    # Print information about the grid search
    logger = logging.getLogger(__name__)
    logger.info(f"Using {gs.n_splits_} splits, {type(gs.best_estimator_).__name__} got a best validation score of {gs.best_score_:4f} +- {gs.cv_results_['std_test_score'][gs.best_index_]}, with the parameters {gs.best_params_}.")

    return gs

def grid_search_ElasticNet(X_train: pd.DataFrame, y_train: pd.Series) -> GridSearchCV:
    """Trains the ElasticNet regression model through a gridsearch crossvalidation to get
    the best hyper parameters.

    Args:
        X_train: Training data of features.
        y_train: Training data for output.

    Returns:
        Trained model.
    """
    gs_params = {
        'alpha': np.logspace(-1, 1, 10),
        'l1_ratio': np.arange(0, 1, 0.1),
    }
    gs = GridSearchCV(ElasticNet(random_state=0), gs_params, cv=5, verbose=3)
    gs.fit(X_train, y_train)

    # Print information about the grid search
    logger = logging.getLogger(__name__)
    logger.info(f"Using {gs.n_splits_} splits, {type(gs.best_estimator_).__name__} got a best validation score of {gs.best_score_:4f} +- {gs.cv_results_['std_test_score'][gs.best_index_]}, with the parameters {gs.best_params_}.")

    return gs

def grid_search_random_forest_regressor(X_train: pd.DataFrame, y_train: pd.Series) -> GridSearchCV:
    """Trains a random forest regression model through a gridsearch crossvalidation to get
    the best hyper parameters.

    Args:
        X_train: Training data of features.
        y_train: Training data for output.

    Returns:
        Trained model.
    """
    gs_params = {
        'n_estimators': np.arange(50, 250, 25),
        'max_depth': list(np.arange(2, 8, 1)) + [None],
    }
    gs = GridSearchCV(RandomForestRegressor(random_state=0), gs_params, cv=5, verbose=3)
    gs.fit(X_train, y_train)

    # Print information about the grid search
    logger = logging.getLogger(__name__)
    logger.info(f"Using {gs.n_splits_} splits, {type(gs.best_estimator_).__name__} got a best validation score of {gs.best_score_:4f} +- {gs.cv_results_['std_test_score'][gs.best_index_]}, with the parameters {gs.best_params_}.")

    return gs

def grid_search_gradient_boosting_regressor(X_train: pd.DataFrame, y_train: pd.Series) -> GridSearchCV:
    """Trains a gradient boosting regression model through a gridsearch crossvalidation to get
    the best hyper parameters.

    Args:
        X_train: Training data of features.
        y_train: Training data for output.

    Returns:
        Trained model.
    """
    gs_params = {
        'n_estimators': np.arange(50, 500, 50),
        'learning_rate': np.logspace(-2, 0, 5),
        'max_depth': [3],
        'n_iter_no_change': [50],
    }
    gs = GridSearchCV(GradientBoostingRegressor(random_state=0), gs_params, cv=5, verbose=3)
    gs.fit(X_train, y_train)

    # Print information about the grid search
    logger = logging.getLogger(__name__)
    logger.info(f"Using {gs.n_splits_} splits, {type(gs.best_estimator_).__name__} got a best validation score of {gs.best_score_:4f} +- {gs.cv_results_['std_test_score'][gs.best_index_]}, with the parameters {gs.best_params_}.")

    return gs

def evaluate_grid_searched_model(gs: GridSearchCV, X_test: pd.DataFrame, y_test: pd.Series):
    """Calculates and logs the score.

    Args:
        model: Trained model.
        X_test: Testing data of features.
        y_test: Testing data of outputs.
    """
    logger = logging.getLogger(__name__)
    logger.info(f"{type(gs.best_estimator_).__name__} has an accuracy of {gs.score(X_test, y_test):.4f} on test data.")