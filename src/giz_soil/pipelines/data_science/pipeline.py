from kedro.pipeline import Pipeline, node

from .nodes import *

def create_pipeline(model: str):
    # General
    split_yield_prediction_model_input_node = node(
        func=split_data,
        inputs=["yield_prediction_model_input", "params:yield_prediction_model_input"],
        outputs=["X_train", "X_test", "y_train", "y_test"],
        name="split_yield_prediction_model_input",
    )

    normalize_data_node = node(
        func=normalize_data,
        inputs=["X_train", "X_test", "params:yield_prediction_model_input"],
        outputs=["X_train_normalized", "X_test_normalized"],
        name="normalize_data",
    )

    # Lasso for yield
    train_lasso_regression_for_yield_prediction_node = node(
        func=grid_search_lasso,
        inputs=["X_train_normalized", "y_train"],
        outputs="lasso_regression_for_yield_prediction",
        name="train_lasso_regression_for_yield_prediction",
    )

    evaluate_lasso_regression_for_yield_prediction_node = node(
        func=evaluate_grid_searched_model,
        inputs=["lasso_regression_for_yield_prediction", "X_test_normalized", "y_test"],
        outputs=None,
        name="evaluate_lasso_regression_for_yield_prediction",
    )

    # ElasticNet for yield
    train_ElasticNet_regression_for_yield_prediction_node = node(
        func=grid_search_ElasticNet,
        inputs=["X_train_normalized", "y_train"],
        outputs="ElasticNet_regression_for_yield_prediction",
        name="train_ElasticNet_regression_for_yield_prediction",
    )

    evaluate_ElasticNet_regression_for_yield_prediction_node = node(
        func=evaluate_grid_searched_model,
        inputs=["ElasticNet_regression_for_yield_prediction", "X_test_normalized", "y_test"],
        outputs=None,
        name="evaluate_ElasticNet_regression_for_yield_prediction",
    )

    # RandomForestRegressor for yield
    train_random_forest_regression_for_yield_prediction_node = node(
        func=grid_search_random_forest_regressor,
        inputs=["X_train_normalized", "y_train"],
        outputs="random_forest_regression_for_yield_prediction",
        name="train_random_forest_regression_for_yield_prediction",
    )

    evaluate_random_forest_regression_for_yield_prediction_node = node(
        func=evaluate_grid_searched_model,
        inputs=["random_forest_regression_for_yield_prediction", "X_test_normalized", "y_test"],
        outputs=None,
        name="evaluate_random_forest_regression_for_yield_prediction",
    )

    # GradientBoostingRegressor for yield
    train_gradient_boosting_regression_for_yield_prediction_node = node(
        func=grid_search_gradient_boosting_regressor,
        inputs=["X_train_normalized", "y_train"],
        outputs="gradient_boosting_regression_for_yield_prediction",
        name="train_gradient_boosting_regression_for_yield_prediction",
    )

    evaluate_gradient_boosting_regression_for_yield_prediction_node = node(
        func=evaluate_grid_searched_model,
        inputs=["gradient_boosting_regression_for_yield_prediction", "X_test_normalized", "y_test"],
        outputs=None,
        name="evaluate_gradient_boosting_regression_for_yield_prediction",
    )

   
    if model == "lasso_regression_for_yield_prediction":
        return Pipeline(
            [
                split_yield_prediction_model_input_node,
                normalize_data_node,
                train_lasso_regression_for_yield_prediction_node,
                evaluate_lasso_regression_for_yield_prediction_node,
            ]
        )
    elif model == "ElasticNet_regression_for_yield_prediction":
        return Pipeline(
            [
                split_yield_prediction_model_input_node,
                normalize_data_node,
                train_ElasticNet_regression_for_yield_prediction_node,
                evaluate_ElasticNet_regression_for_yield_prediction_node,
            ]
        )
    elif model == "random_forest_regression_for_yield_prediction":
        return Pipeline(
            [
                split_yield_prediction_model_input_node,
                normalize_data_node,
                train_random_forest_regression_for_yield_prediction_node,
                evaluate_random_forest_regression_for_yield_prediction_node,
            ]
        )
    elif model == "gradient_boosting_regression_for_yield_prediction":
        return Pipeline(
            [
                split_yield_prediction_model_input_node,
                normalize_data_node,
                train_gradient_boosting_regression_for_yield_prediction_node,
                evaluate_gradient_boosting_regression_for_yield_prediction_node,
            ]
        )
    else:
        raise ValueError