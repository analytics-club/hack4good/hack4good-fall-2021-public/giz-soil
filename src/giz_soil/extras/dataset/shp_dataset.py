from pathlib import Path
from kedro.io import AbstractDataSet
import geopandas as geopd



class ShapefileDataSet(AbstractDataSet):
    """
    Dataset reader for the .shp datasets
    """
    def __init__(self, filepath):
        self._filepath = filepath

    def _load(self) -> geopd.GeoDataFrame:
        return geopd.read_file(self._filepath)

    def _save(self, data: geopd.GeoDataFrame) -> None:
        return data.to_file(self._filepath)

    def _exists(self) -> bool:
        return Path(self._filepath.as_posix()).exists()

    def _describe(self):
        return
