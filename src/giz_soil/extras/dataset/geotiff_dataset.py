from pathlib import Path
from kedro.io import AbstractDataSet
import rasterio as rs
import geopandas as geopd




class GeoTIFFDataSet(AbstractDataSet):
    """
    Dataset reader for GeoTIFF files.
    """
    
    def __init__(self, filepath):
        self._filepath = filepath

    def _load(self) -> rs.io.DatasetReader:
        return rs.open(self._filepath)

    def _save(self, image: rs.io.DatasetReader) -> None:
        """
        This was not omplemented as it was not necessary, we only execute reads on the actual file.
        """
        return None
    def _exists(self) -> bool:
        return Path(self._filepath.as_posix()).exists()

    def _describe(self):
        return

