giz\_soil package
=================

.. automodule:: giz_soil
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   giz_soil.extras
   giz_soil.pipelines

Submodules
----------

giz\_soil.hooks module
----------------------

.. automodule:: giz_soil.hooks
   :members:
   :undoc-members:
   :show-inheritance:

giz\_soil.pipeline\_registry module
-----------------------------------

.. automodule:: giz_soil.pipeline_registry
   :members:
   :undoc-members:
   :show-inheritance:

giz\_soil.settings module
-------------------------

.. automodule:: giz_soil.settings
   :members:
   :undoc-members:
   :show-inheritance:
