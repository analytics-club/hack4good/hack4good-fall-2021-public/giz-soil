giz\_soil.extras package
========================

.. automodule:: giz_soil.extras
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   giz_soil.extras.dataset
