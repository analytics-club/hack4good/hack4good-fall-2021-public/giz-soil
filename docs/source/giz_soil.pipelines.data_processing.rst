giz\_soil.pipelines.data\_processing package
============================================

.. automodule:: giz_soil.pipelines.data_processing
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

giz\_soil.pipelines.data\_processing.pipeline module
----------------------------------------------------

.. automodule:: giz_soil.pipelines.data_processing.pipeline
   :members:
   :undoc-members:
   :show-inheritance:
