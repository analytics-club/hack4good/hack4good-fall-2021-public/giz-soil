giz\_soil.extras.dataset package
================================

.. automodule:: giz_soil.extras.dataset
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

giz\_soil.extras.dataset.geotiff\_dataset module
------------------------------------------------

.. automodule:: giz_soil.extras.dataset.geotiff_dataset
   :members:
   :undoc-members:
   :show-inheritance:

giz\_soil.extras.dataset.shp\_dataset module
--------------------------------------------

.. automodule:: giz_soil.extras.dataset.shp_dataset
   :members:
   :undoc-members:
   :show-inheritance:
