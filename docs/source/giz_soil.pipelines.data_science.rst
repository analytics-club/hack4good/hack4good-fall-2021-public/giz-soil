giz\_soil.pipelines.data\_science package
=========================================

.. automodule:: giz_soil.pipelines.data_science
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

giz\_soil.pipelines.data\_science.nodes module
----------------------------------------------

.. automodule:: giz_soil.pipelines.data_science.nodes
   :members:
   :undoc-members:
   :show-inheritance:

giz\_soil.pipelines.data\_science.pipeline module
-------------------------------------------------

.. automodule:: giz_soil.pipelines.data_science.pipeline
   :members:
   :undoc-members:
   :show-inheritance:
