giz\_soil.pipelines package
===========================

.. automodule:: giz_soil.pipelines
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   giz_soil.pipelines.data_processing
   giz_soil.pipelines.data_science
