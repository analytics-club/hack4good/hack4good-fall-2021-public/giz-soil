import pandas as pd
import numpy as np
from sklearn.gaussian_process import GaussianProcessRegressor

import catalogue.dataCatalogue as dc

__measurements = ['cluster_lat', 'cluster_lon', 'pH_2015', 'pH_2019',
       'Cgkg_2015', 'Cgkg_2019', 'Ngkg', 'Pgkg', 'Kmmol+kg', 'Cammol+/kg',
       'Mgmmol+kg', 'ZN mgkg', 'Cumgkg', 'CECmmol+kg', 'AgricLimekg',
       'Compostkg', 'Claygkg', 'Sandgkg', 'Target_yield', 'Suit_potatoes',
       'Suit_Beans', 'Suit_Grains', 'Suit_veges', 'Counter']
__important_measurements = ['cluster_lat', 'cluster_lon', 'pH_2015', 'pH_2019',
       'Cgkg_2015', 'Cgkg_2019', 'Ngkg', 'Pgkg', 'Kmmol+kg', 'Cammol+/kg']
__values_to_transfer = ['pH_2015', 'pH_2019',
       'Cgkg_2015', 'Cgkg_2019', 'Ngkg', 'Pgkg', 'Kmmol+kg', 'Cammol+/kg']

def enrich_dataframe(df_enriched: pd.DataFrame, x_col: str, y_col: str) -> pd.DataFrame:
    """
    @param - df_enriched: Dataframe containing points with geographical coordinates (float) to enhance with soil data.
    @param - x_col: Label of the longitude column
    @param - y_col: Label of the lattitude column

    @return: New dataframe with the additional columns. Original dataframe is unmodified.

    """
    def make_predictors(): 
        """
        Takes the soil dataset and returns a collection of predictors.
        These take geographical coordinate and return the interpolated value for it (feature depending on predictor).
        """
        df_soil = dc.get_dataset('kenya_soil_2015-2019')
        
        #We average all soild characteristics within the same cluster
        dfmeans = df_soil[['Cluster'] + __important_measurements].groupby(by = 'Cluster').mean() 

        #We collect all coordinate points from the soil dataset
        x = dfmeans['cluster_lon']
        y = dfmeans['cluster_lat']
        points = np.vstack([x.to_numpy(), y.to_numpy()]).T

        GP_Regressors = {}
        #For each of the interpolated features we fit our points
        #with the corresponding z
        for key in __values_to_transfer:

            z = dfmeans[key].to_numpy()

            GP = GaussianProcessRegressor(random_state=42)
            GP.fit(points, z)
            
            GP_Regressors[key] = GP
        return GP_Regressors

    
    predictors = make_predictors()

    #We fetch all x,y coordinates on the dataset to enrich
    xpred = df_enriched[x_col]
    ypred = df_enriched[y_col]

    #We filter out all points with a null x or y coordinate
    xmask = xpred.isnull()
    ymask = ypred.isnull()
    mask = (~xmask)*(~ymask)

    points = np.vstack([xpred.to_numpy(), ypred.to_numpy()]).T
    #Selecting the points
    mpoints = points[mask]
    
    res = df_enriched.copy()
    for value_key in __values_to_transfer:
        
        #Points with x or y null will be predicted nan
        pred_value = np.empty(xpred.shape)
        pred_value[:] = np.nan

        m_pred_value = predictors[value_key].predict(mpoints)

        #Assigning the result in the valid points
        pred_value[mask] = m_pred_value
        res[value_key] = pred_value
    
    return res
