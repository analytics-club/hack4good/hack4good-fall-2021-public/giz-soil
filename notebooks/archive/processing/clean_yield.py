#!/usr/bin/env python
# coding: utf-8
# Original Creator of this file: Afshan
# Migration from Jupyter Notebook to python script: Nando

# ### GIZ Climate Project


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def clean_Maize(Maize_2018, Maize_2019):
    #merging the maize files accordingly- creating a new dataframe

    maize_df_2018 = Maize_2018[['Pseudo_ID','Unit ID','section_one/date_interview','section_one/county-name',
                            'section_one/subcounty_name','section_one/ward_name',
                            'section_one/farmer_category','cluster_lat','cluster_lon','Cluster','Distance_to_cluster',
                            'section_two/Give_the_year_when_t_gaged_in_the_project',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/Conservation_A',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/Agroforestry',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/Soil_&_water_c',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/ISFM',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/Push-pull',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/GAP',
                            'section_two/Which_technologies_d_u_apply_in_your_farm/Integrated_Pes',
                            'section_two/crop_acreage',
                            'section_two/crop_var','section_two/crop_pattern',
                            'section_two/fertilizer_app','section_two/fertilizer_type',
                            'section_two/seed_rate','section_three/grain_weight_total']]

    maize_df_2019 = Maize_2019[['Pseudo_ID','Unit ID','group_ld4mw96/date_interview','group_ld4mw96/County_name','group_ld4mw96/sub_county_001',
                            'group_ld4mw96/ward','group_ld4mw96/farmer_category','cluster_lat','cluster_lon',
                            'Cluster','Distance_to_cluster','group_ld4mw96/year_first_engaged',
                            'group_by6ib65/practised_technology/conservation_a','group_by6ib65/practised_technology/agroforestry',
                            'group_by6ib65/practised_technology/soil___water_c','group_by6ib65/practised_technology/integrated_soi',
                            'group_by6ib65/practised_technology/push_pull','group_by6ib65/practised_technology/good_agronomic',
                            'group_by6ib65/practised_technology/none',
                            'group_by6ib65/farm_area_maize',
                            'group_by6ib65/maize_variety','group_by6ib65/cropping_pattern',
                            'group_by6ib65/fertilizer_applicati','group_by6ib65/fertilizer_type',
                            'group_by6ib65/seed_rate_used',
                            'group_th5on78/total_weight']]

    maize_df_2019.insert(2,"group",'6ib65')
    maize_df_2019.insert(2,"time",'2019')
    # maize_df_2019.insert(20,"Isf_Pest_Management","False")
    maize_df_2019.insert(20,"Isf_Pest_Management", np.nan)

    maize_df_2018.insert(2,"group",'')
    maize_df_2018.insert(2,"time",'2018')
    # maize_df_2018.insert(21,"No_Technology_Practiced/Other", np.nan)
    # maize_df_2018.insert(21,"No_Technology_Practiced/Other", "false")
    maize_df_2018.insert(21,"No_Technology_Practiced/Other", np.nan)

    maize_df_2018.columns=["Pseudo_ID","Unit_ID","Time","Group","Date_of_Interview","County","Sub_County","Ward","Farmer_Category",'cluster_lat',"cluster_lon","Cluster_No.","Distance_to_cluster","Year_of_Engagement","conservation_agriculture",'Agroforestry','Soil_Water_conservation_meas',"Integrated_Soil_Fertility","Push_Pull","Good_Agronomic_Practices","Isf_Pest_Management","No_Technology_Practiced/Other","Total_Area (Acres)","Variety","Cropping_pattern",'Fertilizer_application','Fertilizer_type','Seed_Rate_used','Yield_Harvest_Measure']
    maize_df_2019.columns=["Pseudo_ID","Unit_ID","Time","Group","Date_of_Interview","County","Sub_County","Ward","Farmer_Category",'cluster_lat',"cluster_lon","Cluster_No.","Distance_to_cluster","Year_of_Engagement","conservation_agriculture",'Agroforestry','Soil_Water_conservation_meas',"Integrated_Soil_Fertility","Push_Pull","Good_Agronomic_Practices","Isf_Pest_Management","No_Technology_Practiced/Other","Total_Area (Acres)","Variety","Cropping_pattern",'Fertilizer_application','Fertilizer_type','Seed_Rate_used','Yield_Harvest_Measure']

    maize_df=pd.concat([maize_df_2019,maize_df_2018])
    maize_df.insert(4,"Crop_Type",'Maize') 
    return maize_df

def clean_Beans(Beans_2018, Beans_2019, Beans_2019_2):
    #merging the beans files accordingly- creating a new dataframe

    beans_df_2019 = Beans_2019[['Pseudo_ID','Unit ID','group_hc6ip72/county','group_hc6ip72/sub_County','group_hc6ip72/ward',
                        'group_hc6ip72/farmer_category','cluster_lat','cluster_lon','Cluster','Distance_to_cluster',
                        'group_hc6ip72/year_first_engaged',
                        'group_ly5oh23/technology_practiced/conservation_agriculture',
                        'group_ly5oh23/technology_practiced/agroforestry',
                        'group_ly5oh23/technology_practiced/soil___water_conservation_meas',
                        'group_ly5oh23/technology_practiced/integrated_soil_fertility___pe',
                        'group_ly5oh23/technology_practiced/push_pull',
                        'group_ly5oh23/technology_practiced/good_agronomic_practices',
                        'group_ly5oh23/technology_practiced/none',
                        'group_ly5oh23/total_area_beans_acre',
                        'group_ly5oh23/variety_of_beans',
                        'group_ly5oh23/cropping_pattern',
                        'group_ly5oh23/fertilizer_application',
                        'group_ly5oh23/type_fertilizer',
                        'group_ly5oh23/seed_rate_used',
                        'group_fe3jk86/yield_harvested_kg']]

    beans_df_2019.insert(2,"group",'hc6ip72')
    beans_df_2019.insert(2,"time",'2019')
    beans_df_2019.insert(18,"Isf_Pest_Management", np.nan)
    # beans_df_2019.insert(18,"Isf_Pest_Management", False)
    beans_df_2019.columns=["Pseudo_ID","Unit_ID","Time","Group","County","Sub_County","Ward","Farmer_Category",'cluster_lat',"cluster_lon","Cluster_No.","Distance_to_cluster","Year_of_Engagement","conservation_agriculture",'Agroforestry','Soil_Water_conservation_meas',"Integrated_Soil_Fertility","Push_Pull","Good_Agronomic_Practices","Isf_Pest_Management","No_Technology_Practiced/Other","Total_Area (Acres)","Variety","Cropping_pattern",'Fertilizer_application','Fertilizer_type','Seed_Rate_used','Yield_Harvest_Measure']

    beans_df_2019_2 = Beans_2019_2[['Pseudo_ID','Unit ID','group_hc6ip72/county','group_hc6ip72/sub_County','group_hc6ip72/ward',
                        'group_hc6ip72/farmer_category','cluster_lat','cluster_lon','Cluster','Distance_to_cluster',
                        'group_hc6ip72/year_first_engaged',
                        'group_ly5oh23/technology_practiced/conservation_agriculture',
                        'group_ly5oh23/technology_practiced/agroforestry',
                        'group_ly5oh23/technology_practiced/soil___water_conservation_meas',
                        'group_ly5oh23/technology_practiced/integrated_soil_fertility___pe',
                        'group_ly5oh23/technology_practiced/push_pull',
                        'group_ly5oh23/technology_practiced/good_agronomic_practices',
                        'group_ly5oh23/technology_practiced/none',
                        'group_ly5oh23/total_area_beans_acre',
                        'group_ly5oh23/variety_of_beans',
                        'group_ly5oh23/cropping_pattern',
                        'group_ly5oh23/fertilizer_application',
                        'group_ly5oh23/type_fertilizer',
                        'group_ly5oh23/seed_rate_used',
                        'group_fe3jk86/yield_harvested_kg']]

    beans_df_2019_2.insert(2,"group",'hc6ip72')
    beans_df_2019_2.insert(2,"time",'2019')
    beans_df_2019_2.insert(18,"Isf_Pest_Management", np.nan)
    # beans_df_2019_2.insert(18,"Isf_Pest_Management", False)
    beans_df_2019_2.columns=["Pseudo_ID","Unit_ID","Time","Group","County","Sub_County","Ward","Farmer_Category",'cluster_lat',"cluster_lon","Cluster_No.","Distance_to_cluster","Year_of_Engagement","conservation_agriculture",'Agroforestry','Soil_Water_conservation_meas',"Integrated_Soil_Fertility","Push_Pull","Good_Agronomic_Practices","Isf_Pest_Management","No_Technology_Practiced/Other","Total_Area (Acres)","Variety","Cropping_pattern",'Fertilizer_application','Fertilizer_type','Seed_Rate_used','Yield_Harvest_Measure']



    beans_df_2018 = Beans_2018[['Pseudo_ID','Unit ID','group_hi5kl13/county',
                            'group_hi5kl13/sub_county','group_hi5kl13/ward',
                            'group_hi5kl13/farmer_category','cluster_lat','cluster_lon','Cluster',
                            'Distance_to_cluster','group_hi5kl13/year_engaged_with_project',
                            'group_yt0lm94/technologies_applied/conservation_agriculture',
                            'group_yt0lm94/technologies_applied/agroforestry',
                            'group_yt0lm94/technologies_applied/soil___water_conservation',
                            'group_yt0lm94/technologies_applied/push_pull',
                            'group_yt0lm94/technologies_applied/good_agronomic_practices',
                            'group_yt0lm94/technologies_applied/isf___pest_management',
                            'group_yt0lm94/technologies_applied/other',
                            'group_yt0lm94/plot_area_beans','group_yt0lm94/beans_variety',
                            'group_yt0lm94/cropping_pattern','group_yt0lm94/fertilizer_applied',
                            'group_yt0lm94/name_fertilizer','group_yt0lm94/seed_rate_use',
                            'group_om3pb56/beans_yield_measure']]

    beans_df_2018.insert(2,"group",'hi5kl13')
    beans_df_2018.insert(2,"time",'2018')
    beans_df_2018.insert(13,"Integrated_Soil_Fertility", np.nan)
    # beans_df_2018.insert(13,"Integrated_Soil_Fertility", False)
    beans_df_2018.columns=["Pseudo_ID","Unit_ID","Time","Group","County","Sub_County","Ward","Farmer_Category",'cluster_lat',"cluster_lon","Cluster_No.","Distance_to_cluster","Year_of_Engagement","conservation_agriculture",'Agroforestry','Soil_Water_conservation_meas',"Integrated_Soil_Fertility","Push_Pull","Good_Agronomic_Practices","Isf_Pest_Management","No_Technology_Practiced/Other","Total_Area (Acres)","Variety","Cropping_pattern",'Fertilizer_application','Fertilizer_type','Seed_Rate_used','Yield_Harvest_Measure']

    beans_df=pd.concat([beans_df_2019, beans_df_2019_2, beans_df_2018])
    beans_df.insert(4,"Crop_Type",'Beans')

    return beans_df


if __name__ == '__main__':
    ### Example of usage of these functions ###

    #importing the files
    Maize_2019=pd.read_csv("data/ProtectedData/01_raw_data/2 Yield Data Maize 2018-19/2019/Maize yield assessment _July_Aug2019 dataset.csv")
    Maize_2018=pd.read_csv("data/ProtectedData/01_raw_data/2 Yield Data Maize 2018-19/2018/maize yield assessment_July 2018 dataset.csv")

    Beans_2019=pd.read_csv("data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2019/Beans ass dataset _Dec 2019.csv")
    Beans_2019_2=pd.read_csv("data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2019/Beans assessment dataset _July 2019.csv")
    Beans_2018=pd.read_csv("data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2018/Beans assessment dataset _Dec 2018.csv")

    # Executing Functions 
    beans_df = clean_Beans(Beans_2018, Beans_2019, Beans_2019_2)
    maize_df = clean_Maize(Maize_2018, Maize_2019)

    beans_df.head()
    maize_df.head()