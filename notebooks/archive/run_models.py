

import numpy as np
import sklearn
import pandas as pd

from processing.clean_yield import clean_Beans, clean_Maize

from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.model_selection import cross_validate
from sklearn.impute import SimpleImputer,KNNImputer 

from processing.enrichment.geospatial.soil import enrich_dataframe

from catalogue import dataCatalogue 
# import processing.enrichment.geospatial.soil as soil

def run_ml_model():

    #importing the raw files
    # Maize_2019 = dataCatalogue("maize_july-aug_2019")
    # Maize_2018 = dataCatalogue("maize_july-aug_2018")

    # Beans_2019_2 = dataCatalogue('beans_july_2019')
    # Beans_2019 = dataCatalogue('beans_dec_2019')
    # Beans_2018 = dataCatalogue('kenya_soil_2015-2019')

    # Soil_survey = dataCatalogue("kenya_soil_2015-2019")

    Maize_2019=pd.read_csv("../data/ProtectedData/01_raw_data/2 Yield Data Maize 2018-19/2019/Maize yield assessment _July_Aug2019 dataset.csv")
    Maize_2018=pd.read_csv("../data/ProtectedData/01_raw_data/2 Yield Data Maize 2018-19/2018/maize yield assessment_July 2018 dataset.csv")

    Beans_2019_2=pd.read_csv("../data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2019/Beans assessment dataset _July 2019.csv")
    Beans_2019=pd.read_csv("../data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2019/Beans ass dataset _Dec 2019.csv")
    Beans_2018=pd.read_csv("../data/ProtectedData/01_raw_data/3 Yield Data Beans 2018-19/2018/Beans assessment dataset _Dec 2018.csv")

    # Executing Functions 
    beans_df = clean_Beans(Beans_2018, Beans_2019, Beans_2019_2)
    maize_df = clean_Maize(Maize_2018, Maize_2019)

    feature_names = ['Crop_Type', 'Farmer_Category',  'cluster_lat', 'cluster_lon', 
        'Year_of_Engagement', 'Integrated_Soil_Fertility', 'Total_Area (Acres)',
        #'Variety',
        'Cropping_pattern', 'Fertilizer_application',
        # 'Fertilizer_type',
        'Seed_Rate_used', 'Yield_Harvest_Measure'
    ]

    categorical_features = [
        'Crop_Type', 'Farmer_Category',
        'Integrated_Soil_Fertility', 'Variety',
        'Cropping_pattern', 'Fertilizer_application', 'Fertilizer_type'
    ]

    label_names = [
        'conservation_agriculture', 'Agroforestry', 'Soil_Water_conservation_meas',
        'Push_Pull', 'Good_Agronomic_Practices', 'Isf_Pest_Management', 
    ]


    out_df = enrich_dataframe(maize_df, "cluster_lon", "cluster_lat")


    # merge the bean and maize data at this point 
    maize_df = maize_df[feature_names+label_names]
    bean_df = beans_df[feature_names+label_names]

    merge_df = pd.concat([maize_df, bean_df])
    Y = merge_df[label_names].replace({True: 1, False: 0})
    feat_df = merge_df[feature_names]

    # convert categorical features to dummies
    catfeats = []
    feats = []
    for f in feature_names:
        if f in categorical_features:
            feats.append(pd.get_dummies(feat_df[f]))
        else:
            feats.append(feat_df[f])

    X = pd.concat(feats,axis=1).to_numpy()


    # we need to find a solution to fix the missing values, as a trial, we use KNN here
    imputer = KNNImputer(n_neighbors=2, weights="uniform")
    X = imputer.fit_transform(X)

    assert(not np.isnan(X).any())

    for yname in label_names:
        print("**********************************")
        print("Target name: ", yname)
        
        y = Y[yname].to_numpy()#.astype(np.bool)
        valid_mask = ~np.isnan(y)
        this_X, this_y = X[valid_mask], y[valid_mask]

        print("Num available samples: ", this_X.shape[0])
        print("Label balance (binary): ", this_y.sum() / this_y.shape[0])
        
        RFcv = RandomForestClassifier(n_estimators=100).fit(this_X,this_y)
        cv_results = cross_validate(RFcv, this_X, this_y, cv=50)
        print("Mean Acc. test_score: ", cv_results["test_score"].mean()) 

        pass

    return None

if __name__ == '__main__':
    run_ml_model()